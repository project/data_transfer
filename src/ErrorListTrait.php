<?php

namespace Drupal\data_transfer;

/**
 * Provides trait for a generic error list.
 *
 * @see \Drupal\data_transfer\ErrorListInterface
 */
trait ErrorListTrait {

  /**
   * The validation errors.
   *
   * @var array
   */
  protected $errors = [];

  /**
   * {@inheritdoc}
   */
  public function getErrors(): array {
    return $this->errors;
  }

  /**
   * {@inheritdoc}
   */
  public function hasErrors(): bool {
    return !empty($this->errors);
  }

  /**
   * {@inheritdoc}
   */
  public function hasErrorsAt(string $path): bool {
    if (!$this->hasErrors()) {
      return FALSE;
    }

    if (isset($this->errors[$path])) {
      return TRUE;
    }

    $matching_keys = preg_grep(
      '@^' . preg_quote($path . '.', '@') . '@',
      array_keys($this->errors)
    );
    return !empty($matching_keys);
  }

  /**
   * {@inheritdoc}
   */
  public function addErrors(array $errors): ErrorListInterface {
    foreach ($errors as $item_path => $item_errors) {
      $existing_errors = $this->errors[$item_path] ?? NULL;
      if (isset($existing_errors)) {
        // Can't just cast to array, because error may be a formatted markup
        // object and it would cast this object to array instead of wrapping it.
        if (!is_array($existing_errors)) {
          $existing_errors = [$existing_errors];
        }
        if (!is_array($item_errors)) {
          $item_errors = [$item_errors];
        }

        $this->errors[$item_path] = array_merge($existing_errors, $item_errors);
      }
      else {
        $this->errors[$item_path] = $item_errors;
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addErrorsAt(
    string $path_prefix,
    array $errors
  ): ErrorListInterface {
    $errors = $this->prefixErrorPaths($path_prefix, $errors);
    return $this->addErrors($errors);
  }

  /**
   * Prefixes the path of all the passed errors.
   *
   * @param string $path_prefix
   *   The path prefix to add.
   * @param array $errors
   *   The list of errors.
   *
   * @return array
   *   The list of errors with paths prefixed.
   */
  protected function prefixErrorPaths(
    string $path_prefix,
    array $errors
  ): array {
    $prefixed_errors = [];

    foreach ($errors as $item_path => $item_errors) {
      if ($item_path === '') {
        $prefixed_path = $path_prefix;
      }
      else {
        $prefixed_path = "{$path_prefix}.{$item_path}";
      }

      $prefixed_errors[$prefixed_path] = $item_errors;
    }

    return $prefixed_errors;
  }

  /**
   * {@inheritdoc}
   */
  public function addPrefixToErrorPaths(
    string $path_prefix
  ): ErrorListInterface {
    $this->errors = $this->prefixErrorPaths($path_prefix, $this->errors);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addError($error): ErrorListInterface {
    return $this->addErrors([
      '' => $error,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function addErrorAt(string $path, $error): ErrorListInterface {
    return $this->addErrors([
      $path => $error,
    ]);
  }

}
