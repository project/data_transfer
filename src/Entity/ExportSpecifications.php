<?php

namespace Drupal\data_transfer\Entity;

/**
 * Dictionary class with export specifications.
 */
final class ExportSpecifications {

  /**
   * The quota quantity min/max export specification.
   */
  public const QUOTA_QUANTITY_MIN_MAX_RULES = 'csv_quota_quantity_min_max_rules';

  /**
   * The quota quantity min/max export specification.
   */
  public const QUOTA_QUANTITY_CLIENT_RULES = 'csv_quota_quantity_client_rules';

}
