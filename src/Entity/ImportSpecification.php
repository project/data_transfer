<?php

namespace Drupal\data_transfer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\data_transfer\Plugin\PluginSpecification;
use Drupal\data_transfer\Plugin\PluginSpecificationInterface;

/**
 * The import specification.
 *
 * @ConfigEntityType(
 *   id = "data_transfer_import_spec",
 *   label = @Translation("Import specification"),
 *   handlers = {},
 *   config_prefix = "import.specification",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "batch",
 *     "reading",
 *     "mapping",
 *     "processing",
 *     "writing",
 *     "logging",
 *     "tags",
 *   },
 * )
 *
 * @see \Drupal\data_transfer\Entity\ExportSpecificationInterface
 *
 * @todo Implement calculateDependencies()
 */
class ImportSpecification extends ConfigEntityBase implements ImportSpecificationInterface {

  /**
   * Default number of records to process per batch operation.
   */
  public const DEFAULT_BATCH_RECORDS_LIMIT = 50;

  /**
   * The entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The import label.
   *
   * @var string
   */
  protected $label;

  /**
   * The import reader plugin ID and settings.
   *
   * @var array
   */
  protected $reading = [];

  /**
   * The import mapper plugin ID and settings.
   *
   * @var array
   */
  protected $mapping = [];

  /**
   * The list of processor plugin IDs and their settings.
   *
   * @var array
   */
  protected $processing = [];

  /**
   * The writer plugin ID and settings.
   *
   * @var array
   */
  protected $writing = [];

  /**
   * The logger plugin ID and settings.
   *
   * @var array
   */
  protected $logging = [];

  /**
   * The batch options.
   *
   * @var array
   */
  protected $batch = [];

  /**
   * The specification tags.
   *
   * @var array
   */
  protected $tags = [];

  /**
   * {@inheritdoc}
   */
  public function getReaderPluginSpecification(): PluginSpecificationInterface {
    return new PluginSpecification(
      $this->reading['reader'],
      $this->reading['settings'] ?? []
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getMapperPluginSpecification(): PluginSpecificationInterface {
    return new PluginSpecification(
      $this->mapping['mapper'],
      $this->mapping['settings'] ?? []
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessorPluginSpecifications(): array {
    $result = [];
    foreach ($this->processing as $processor_data) {
      $result[] = new PluginSpecification(
        $processor_data['processor'],
        $processor_data['settings'] ?? []
      );
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getWriterPluginSpecification(): PluginSpecificationInterface {
    return new PluginSpecification(
      $this->writing['writer'],
      $this->writing['settings'] ?? []
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getLoggerPluginSpecification(): PluginSpecificationInterface {
    return new PluginSpecification(
      $this->logging['logger'],
      $this->logging['settings'] ?? []
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBatchRecordsLimit(): int {
    return $this->batch['limit'] ?? static::DEFAULT_BATCH_RECORDS_LIMIT;
  }

  /**
   * {@inheritdoc}
   */
  public function getTags(): array {
    return $this->tags;
  }

}
