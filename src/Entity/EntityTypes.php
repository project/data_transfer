<?php

namespace Drupal\data_transfer\Entity;

final class EntityTypes {

  public const EXPORT_SPECIFICATION = 'data_transfer_export_spec';

  public const IMPORT_SPECIFICATION = 'data_transfer_import_spec';

  public const FILE = 'file';

}
