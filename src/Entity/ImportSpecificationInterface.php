<?php

namespace Drupal\data_transfer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\data_transfer\Plugin\PluginSpecificationInterface;

/**
 * The import specification.
 *
 * It contains all the plugin specifications necessary to read the source data,
 * process it and write to the storage.
 */
interface ImportSpecificationInterface extends ConfigEntityInterface {

  /**
   * Returns the reader plugin specification.
   *
   * @return \Drupal\data_transfer\Plugin\PluginSpecificationInterface
   *   The plugin specification.
   */
  public function getReaderPluginSpecification(): PluginSpecificationInterface;

  /**
   * Returns the mapper plugin specification.
   *
   * @return \Drupal\data_transfer\Plugin\PluginSpecificationInterface
   *   The plugin specification.
   */
  public function getMapperPluginSpecification(): PluginSpecificationInterface;

  /**
   * Returns the list of processor plugin specifications.
   *
   * @return \Drupal\data_transfer\Plugin\PluginSpecificationInterface[]
   *   The processor plugin specifications.
   */
  public function getProcessorPluginSpecifications(): array;

  /**
   * Returns the writer plugin specification.
   *
   * @return \Drupal\data_transfer\Plugin\PluginSpecificationInterface
   *   The plugin specification.
   */
  public function getWriterPluginSpecification(): PluginSpecificationInterface;

  /**
   * Returns the logger plugin specification.
   *
   * @return \Drupal\data_transfer\Plugin\PluginSpecificationInterface
   *   The plugin specification.
   */
  public function getLoggerPluginSpecification(): PluginSpecificationInterface;

  /**
   * Returns the number of records that should be processed per run.
   *
   * @return int
   *   The number of records.
   */
  public function getBatchRecordsLimit(): int;

  /**
   * Returns the export specification tags.
   *
   * @return string[]
   *   The export specification tags list.
   */
  public function getTags(): array;

}
