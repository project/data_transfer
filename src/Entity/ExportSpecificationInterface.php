<?php

namespace Drupal\data_transfer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\data_transfer\Plugin\PluginSpecificationInterface;

/**
 * The export specification.
 *
 * It contains all the plugin specifications necessary to load the data,
 * normalize it and write to the export target.
 */
interface ExportSpecificationInterface extends ConfigEntityInterface {

  /**
   * Returns the loader plugin specification.
   *
   * @return \Drupal\data_transfer\Plugin\PluginSpecificationInterface
   *   The plugin specification.
   */
  public function getLoaderPluginSpecification(): PluginSpecificationInterface;

  /**
   * Returns the list of transformer plugin specifications.
   *
   * @return \Drupal\data_transfer\Plugin\PluginSpecificationInterface[]
   *   The transformer plugin specifications.
   */
  public function getTransformerPluginSpecifications(): array;

  /**
   * Returns the list of normalizer plugin specifications.
   *
   * @return \Drupal\data_transfer\Plugin\PluginSpecificationInterface[]
   *   The normalizer plugin specifications.
   */
  public function getNormalizerPluginSpecifications(): array;

  /**
   * Returns the writer plugin specification.
   *
   * @return \Drupal\data_transfer\Plugin\PluginSpecificationInterface
   *   The plugin specification.
   */
  public function getWriterPluginSpecification(): PluginSpecificationInterface;

  /**
   * Returns the number of records that should be processed per run.
   *
   * @return int
   *   The number of records.
   */
  public function getBatchRecordsLimit(): int;

  /**
   * Returns the export specification tags.
   *
   * @return string[]
   *   The export specification tags list.
   */
  public function getTags(): array;

  /**
   * Sets the export specification tags.
   *
   * @param string[] $tags
   *   The export specification tags list.
   *
   * @return static
   */
  public function setTags(array $tags);

}
