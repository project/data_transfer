<?php

namespace Drupal\data_transfer\Entity;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\data_transfer\Plugin\PluginSpecification;
use Drupal\data_transfer\Plugin\PluginSpecificationInterface;

/**
 * The export specification.
 *
 * @ConfigEntityType(
 *   id = "data_transfer_export_spec",
 *   label = @Translation("Export specification"),
 *   handlers = {},
 *   config_prefix = "export.specification",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   config_export = {
 *     "id",
 *     "batch",
 *     "loading",
 *     "transformation",
 *     "normalization",
 *     "writing",
 *     "tags",
 *   },
 * )
 *
 * @see \Drupal\data_transfer\Entity\ExportSpecificationInterface
 */
class ExportSpecification extends ConfigEntityBase implements ExportSpecificationInterface {

  /**
   * Default number of records to process per batch operation.
   */
  public const DEFAULT_BATCH_RECORDS_LIMIT = 50;

  /**
   * The entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The loader plugin ID and settings.
   *
   * @var array
   */
  protected $loading = [];

  /**
   * The list of transformer plugin IDs and their settings.
   *
   * @var array
   */
  protected $transformation = [];

  /**
   * The list of normalizer plugin IDs and their settings.
   *
   * @var array
   */
  protected $normalization = [];

  /**
   * The writer plugin ID and settings.
   *
   * @var array
   */
  protected $writing = [];

  /**
   * The batch options.
   *
   * @var array
   */
  protected $batch = [];

  /**
   * The export specifications tags.
   *
   * @var array
   */
  protected $tags = [];

  /**
   * {@inheritdoc}
   */
  public function getLoaderPluginSpecification(): PluginSpecificationInterface {
    return new PluginSpecification(
      $this->loading['loader'],
      $this->loading['settings'] ?? []
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getTransformerPluginSpecifications(): array {
    $result = [];
    foreach ($this->transformation as $plugin_data) {
      $result[] = new PluginSpecification(
        $plugin_data['transformer'],
        $plugin_data['settings'] ?? []
      );
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getNormalizerPluginSpecifications(): array {
    $result = [];
    foreach ($this->normalization as $normalizer_data) {
      $result[] = new PluginSpecification(
        $normalizer_data['normalizer'],
        $normalizer_data['settings'] ?? []
      );
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getWriterPluginSpecification(): PluginSpecificationInterface {
    return new PluginSpecification(
      $this->writing['writer'],
      $this->writing['settings'] ?? []
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBatchRecordsLimit(): int {
    return $this->batch['limit'] ?? static::DEFAULT_BATCH_RECORDS_LIMIT;
  }

  /**
   * {@inheritdoc}
   */
  public function getTags(): array {
    return $this->tags;
  }

  /**
   * {@inheritdoc}
   */
  public function setTags(array $tags) {
    $this->tags = $tags;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    // @todo Use DI for all plugin manager when possible.
    parent::calculateDependencies();

    /** @var \Drupal\data_transfer\Export\Loader\ExportLoaderPluginManagerInterface $loader_manager */
    $loader_manager = \Drupal::service(
      'plugin.manager.data_transfer_export_loader'
    );
    $plugin_spec = $this->getLoaderPluginSpecification();
    $plugin = $loader_manager->createFromSpecification($plugin_spec);
    if ($plugin instanceof PluginInspectionInterface) {
      $this->calculatePluginDependencies($plugin);
    }

    /** @var \Drupal\data_transfer\Export\Transformer\ExportTransformerPluginManagerInterface $transformer_manager */
    $transformer_manager = \Drupal::service(
      'plugin.manager.data_transfer_export_transformer'
    );
    foreach ($this->getTransformerPluginSpecifications() as $plugin_spec) {
      $plugin = $transformer_manager->createFromSpecification($plugin_spec);
      if ($plugin instanceof PluginInspectionInterface) {
        $this->calculatePluginDependencies($plugin);
      }
    }

    /** @var \Drupal\data_transfer\Export\Normalizer\ExportNormalizerPluginManagerInterface $normalizer_manager */
    $normalizer_manager = \Drupal::service(
      'plugin.manager.data_transfer_export_normalizer'
    );
    foreach ($this->getNormalizerPluginSpecifications() as $plugin_spec) {
      $plugin = $normalizer_manager->createFromSpecification($plugin_spec);
      if ($plugin instanceof PluginInspectionInterface) {
        $this->calculatePluginDependencies($plugin);
      }
    }

    /** @var \Drupal\Component\Plugin\PluginManagerInterface $writer_manager */
    $writer_manager = \Drupal::service(
      'plugin.manager.data_transfer_export_writer'
    );
    $plugin_spec = $this->getWriterPluginSpecification();
    $plugin = $writer_manager->createFromSpecification($plugin_spec);
    if ($plugin instanceof PluginInspectionInterface) {
      $this->calculatePluginDependencies($plugin);
    }
  }

}
