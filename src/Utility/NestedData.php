<?php

namespace Drupal\data_transfer\Utility;

/**
 * Provides helpers to perform operations on nested array/object structures.
 *
 * @see \Drupal\Component\Utility\NestedArray
 */
class NestedData {

  /**
   * Retrieves a value from a nested structure with variable depth.
   *
   * @param mixed $data
   *   The array/object structure from which to get the value.
   * @param array $parents
   *   An array of parent keys of the value, starting with the outermost key.
   * @param bool $key_exists
   *   (optional) Passed by reference and is set to TRUE if the key actually
   *   exists. May be useful to distinguish between missing key and existing
   *   one set to NULL.
   *
   * @return mixed|null
   *   The value or NULL if it's not set.
   */
  public static function getValue(
    $data,
    array $parents,
    bool &$key_exists = NULL
  ) {
    $ref = $data;

    foreach ($parents as $parent) {
      if (is_array($ref)) {
        if (isset($ref[$parent]) || array_key_exists($parent, $ref)) {
          $ref = $ref[$parent];
          continue;
        }
      }
      elseif (is_object($ref)) {
        if (isset($ref->{$parent}) || property_exists($ref, $parent)) {
          $ref = $ref->{$parent};
          continue;
        }
      }

      $key_exists = FALSE;
      return NULL;
    }

    $key_exists = TRUE;
    return $ref;
  }

}
