<?php

namespace Drupal\data_transfer\Batch;

/**
 * Provides base class for various batch generators.
 */
class BatchGeneratorBase {

  /**
   * Provides single operation callback for various callbacks services/methods.
   *
   * @param string $service_name
   *   The callbacks service name to call.
   * @param string $method
   *   The method of the callbacks service to call.
   * @param array $arguments
   *   The arguments to pass to the service method.
   * @param array|\DrushBatchContext $context
   *   The batch context that is passed as last argument to the callbacks
   *   method.
   *
   * @return mixed
   *   Whatever the callbacks instance method returns.
   */
  public static function operation(
    string $service_name,
    string $method,
    array $arguments,
    &$context
  ) {
    // @todo Use service name and method as operation callback when supported.
    $callbacks = \Drupal::service($service_name);
    $callback_args = $arguments;
    $callback_args[] = &$context;
    return call_user_func_array([$callbacks, $method], $callback_args);
  }

  /**
   * The actual batch finish callback that runs method of all callback services.
   *
   * In order for the finish callback to be called on the callbacks class, it
   * must add an item to the results array keyed by the service name and also
   * implement a special interface.
   *
   * @param bool $success
   *   TRUE if the batch was successful.
   * @param array $results
   *   The batch results.
   *
   * @see \Drupal\data_transfer\Batch\FinalizingBatchCallbacksInterface
   */
  public static function finishBatch(
    $success,
    array $results
  ) {
    foreach ($results as $service_name => $result) {
      if (!\Drupal::hasService($service_name)) {
        continue;
      }

      $callbacks = \Drupal::service($service_name);
      if ($callbacks instanceof FinalizingBatchCallbacksInterface) {
        $callbacks->finishBatch($result);
      }
    }
  }

  /**
   * Builds an operation for the batch.
   *
   * @param \Drupal\data_transfer\Batch\BatchCallbacksInterface $callbacks
   *   The callbacks instance.
   * @param string $method
   *   The callbacks method to call.
   * @param array $arguments
   *   The list of arguments to pass to the method.
   *
   * @return array
   *   The batch operation.
   */
  protected function buildOperation(
    BatchCallbacksInterface $callbacks,
    string $method,
    array $arguments
  ) {
    return [
      static::class . '::operation',
      [
        $callbacks->getServiceName(),
        $method,
        $arguments,
      ],
    ];
  }

  /**
   * Starts the batch process and extracts its ID.
   *
   * This method is useful for front API, as it requires the batch ID to call
   * batch endpoints.
   *
   * The batch must be set before calling this method.
   *
   * @return int
   *   The batch ID.
   *
   * @throws \LogicException
   *   Thrown in case there was no batch set before calling this method.
   */
  protected function generateBatchId(): int {
    // Start the processing. For progressive batch it just prepares the batch
    // and assigns an ID.
    batch_process();

    $batch = batch_get();
    $batch_id = $batch['id'] ?? NULL;
    if (!isset($batch_id)) {
      throw new \LogicException('The batch ID was not assigned.');
    }

    return $batch_id;
  }

}
