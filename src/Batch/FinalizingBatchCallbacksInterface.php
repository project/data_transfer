<?php

namespace Drupal\data_transfer\Batch;

/**
 * Defines batch callbacks that requires to do something at the end of batch.
 */
interface FinalizingBatchCallbacksInterface extends BatchCallbacksInterface {

  /**
   * Performs the finalizing.
   *
   * In order for the finish callback to be called, the operation must add
   * an entry to the batch results array with the key of the callbacks service
   * ID and the value that is then passed to the finish callback.
   *
   * @param mixed $result
   *   The result added by the operation.
   *
   * @see \Drupal\data_transfer\Batch\BatchGeneratorBase::finishBatch()
   */
  public function finishBatch($result): void;

}
