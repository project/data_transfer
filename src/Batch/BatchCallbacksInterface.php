<?php

namespace Drupal\data_transfer\Batch;

/**
 * Interface for a base batch callbacks.
 *
 * Batch callbacks are called for executing batch operations and avoid global
 * functions. Their methods are actually called by the batch generator static
 * method.
 *
 * @see \Drupal\data_transfer\Batch\BatchGeneratorBase
 */
interface BatchCallbacksInterface {

  /**
   * Returns service name of current instance.
   *
   * @return string
   *   The service name.
   */
  public function getServiceName(): string;

}
