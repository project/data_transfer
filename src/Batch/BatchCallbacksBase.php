<?php

namespace Drupal\data_transfer\Batch;

/**
 * Provides base class for a set of batch callbacks.
 *
 * @see \Drupal\data_transfer\Batch\BatchCallbacksInterface
 *
 * phpcs:disable Drupal.NamingConventions.ValidVariableName
 * phpcs:disable Drupal.Classes.PropertyDeclaration
 */
abstract class BatchCallbacksBase implements BatchCallbacksInterface {

  /**
   * The service name.
   *
   * This variable is set by the core.
   *
   * @var string
   */
  public $_serviceId;

  /**
   * {@inheritdoc}
   */
  public function getServiceName(): string {
    return $this->_serviceId;
  }

}
