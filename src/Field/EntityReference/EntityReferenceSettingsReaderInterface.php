<?php

namespace Drupal\data_transfer\Field\EntityReference;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Provides methods to read field/storage settings of an entity reference field.
 */
interface EntityReferenceSettingsReaderInterface {

  /**
   * Returns the list of bundle IDs allowed to be referenced from this field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return array
   *   The list of bundle IDs. May be empty for entities having no bundles or
   *   no bundle restrictions selected for the field.
   */
  public function getTargetBundleIds(
    FieldDefinitionInterface $field_definition
  ): array;

  /**
   * Returns the entity type ID of the referenced entity.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return string
   *   The entity type ID.
   */
  public function getTargetEntityTypeId(
    FieldDefinitionInterface $field_definition
  ): string;

}
