<?php

namespace Drupal\data_transfer\Field\EntityReference;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Provides methods to read field/storage settings of an entity reference field.
 */
class EntityReferenceSettingsReader implements EntityReferenceSettingsReaderInterface {

  /**
   * The target entity type ID key in the field definition settings.
   */
  protected const TARGET_TYPE_SETTING = EntityReferenceFieldSettings::TARGET_TYPE;

  /**
   * The handler options key on the field definition settings.
   */
  protected const HANDLER_OPTIONS_SETTING = EntityReferenceFieldSettings::HANDLER_OPTIONS;

  /**
   * The target bundles option of a selection handler.
   */
  protected const TARGET_BUNDLES_OPTION = EntityReferenceFieldSettings::TARGET_BUNDLES_OPTION;

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityTypeId(
    FieldDefinitionInterface $field_definition
  ): string {
    return $field_definition->getFieldStorageDefinition()
      ->getSetting(static::TARGET_TYPE_SETTING);
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetBundleIds(
    FieldDefinitionInterface $field_definition
  ): array {
    // @todo This only supports default selection handler for now.
    $handler_settings = $field_definition
      ->getSetting(static::HANDLER_OPTIONS_SETTING);
    return $handler_settings[static::TARGET_BUNDLES_OPTION] ?? [];
  }

}
