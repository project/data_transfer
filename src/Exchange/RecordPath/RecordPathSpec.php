<?php

namespace Drupal\data_transfer\Exchange\RecordPath;

/**
 * Represents path specification in the import/export record.
 *
 * @see \Drupal\data_transfer\Exchange\RecordPath\RecordPathSpecInterface
 */
class RecordPathSpec implements RecordPathSpecInterface {

  /**
   * The flag for taking value from the record target.
   *
   * @var bool
   */
  protected $fromTarget;

  /**
   * The path in the record source or target.
   *
   * @var array
   */
  protected $path;

  /**
   * A constructor.
   *
   * @param array $path
   *   The path.
   * @param bool $from_target
   *   TRUE for the target path, FALSE for the source one.
   */
  public function __construct(array $path, bool $from_target = FALSE) {
    $this->path = $path;
    $this->fromTarget = $from_target;
  }

  /**
   * {@inheritdoc}
   */
  public function isFromTarget(): bool {
    return $this->fromTarget;
  }

  /**
   * {@inheritdoc}
   */
  public function getPath(): array {
    return $this->path;
  }

}
