<?php

namespace Drupal\data_transfer\Exchange\RecordPath;

/**
 * Provides parser of a path in the import/export record.
 */
class RecordPathParser implements RecordPathParserInterface {

  /**
   * {@inheritdoc}
   */
  public function parsePathSpec(string $path): RecordPathSpecInterface {
    $from_target = FALSE;
    if ($path[0] === static::TARGET_AS_SOURCE_PREFIX) {
      $from_target = TRUE;
      $path = substr($path, 1);
    }
    $path = $this->parsePath($path);
    return new RecordPathSpec($path, $from_target);
  }

  /**
   * {@inheritdoc}
   */
  public function parsePath(string $path): array {
    return explode(static::PATH_DELIMITER, $path);
  }

  /**
   * {@inheritdoc}
   */
  public function pathSpecToString(RecordPathSpecInterface $spec): string {
    $result = $spec->isFromTarget() ? static::TARGET_AS_SOURCE_PREFIX : '';;
    $result .= implode(static::PATH_DELIMITER, $spec->getPath());
    return $result;
  }

}
