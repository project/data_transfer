<?php

namespace Drupal\data_transfer\Exchange\RecordPath;

/**
 * Represents path specification in the import/export record.
 *
 * It allows reading from either source or target of the record without being
 * aware of the details.
 */
interface RecordPathSpecInterface {

  /**
   * Checks if the value should be taken from the record target.
   *
   * @return bool
   *   TRUE if the path is for the record target, FALSE if it's for the source.
   */
  public function isFromTarget(): bool;

  /**
   * Returns the path of the value in the record source or target.
   *
   * @return array
   *   The path.
   */
  public function getPath(): array;

}
