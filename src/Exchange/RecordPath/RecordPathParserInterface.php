<?php

namespace Drupal\data_transfer\Exchange\RecordPath;

/**
 * Provides parser of a path in the import/export record.
 */
interface RecordPathParserInterface {

  /**
   * The property path delimiter.
   */
  public const PATH_DELIMITER = '.';

  /**
   * The optional prefix of the source path to read value from the target.
   */
  public const TARGET_AS_SOURCE_PREFIX = '@';

  /**
   * Parses path in the import/export record.
   *
   * It could be either source or target path, the method isn't actually aware
   * of it. The path components are separated with a "." dot.
   *
   * @param string $path
   *   The path to parse.
   *
   * @return array
   *   The parsed path in source or target of the record.
   */
  public function parsePath(string $path): array;

  /**
   * Parses the path specification.
   *
   * In case the passed path starts with "@" sign, it points to the target in
   * the record, otherwise it's source path.
   *
   * @param string $path
   *   The path specification to parse.
   *
   * @return \Drupal\data_transfer\Exchange\RecordPath\RecordPathSpecInterface
   *   The path specification.
   */
  public function parsePathSpec(string $path): RecordPathSpecInterface;

  /**
   * Converts path spec back to its string representation.
   *
   * @param \Drupal\data_transfer\Exchange\RecordPath\RecordPathSpecInterface $spec
   *   The path spec to convert.
   *
   * @return string
   *   The string representation.
   */
  public function pathSpecToString(RecordPathSpecInterface $spec): string;

}
