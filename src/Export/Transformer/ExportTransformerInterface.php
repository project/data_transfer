<?php

namespace Drupal\data_transfer\Export\Transformer;

/**
 * Transforms a set of records according to its purpose.
 *
 * Transformers are able to add/remove records to the data and receive current
 * set of records at once, while normalizers are only supposed to work with a
 * single record of the data. Transformation happens before normalization.
 */
interface ExportTransformerInterface {

  /**
   * Transforms a set of records.
   *
   * Transformer is allowed to add, remove or change the data records.
   *
   * @param array $data
   *   A set of records to transform.
   */
  public function transform(array &$data): void;

}
