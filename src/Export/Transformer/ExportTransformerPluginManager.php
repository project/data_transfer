<?php

namespace Drupal\data_transfer\Export\Transformer;

use Drupal\data_transfer\Plugin\PluginManagerBase;

/**
 * Plugin manager of the export data transformers.
 */
class ExportTransformerPluginManager extends PluginManagerBase implements ExportTransformerPluginManagerInterface {

  /**
   * {@inheritdoc}
   */
  public const SUBDIR = 'Plugin/data_transfer/ExportTransformer';

  /**
   * {@inheritdoc}
   */
  public const CACHE_KEY = 'export_transformer';

  /**
   * {@inheritdoc}
   */
  public const PLUGIN_INTERFACE = ExportTransformerInterface::class;

}
