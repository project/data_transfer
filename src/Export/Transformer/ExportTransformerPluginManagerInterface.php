<?php

namespace Drupal\data_transfer\Export\Transformer;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\data_transfer\Plugin\PluginSpecificationInterface;

/**
 * Plugin manager of the export data transformers.
 */
interface ExportTransformerPluginManagerInterface extends PluginManagerInterface {

  /**
   * Creates plugin instance from its specification.
   *
   * @param \Drupal\data_transfer\Plugin\PluginSpecificationInterface $specification
   *   The plugin specification.
   * @param array $input
   *   The input to add to the plugin configuration.
   *
   * @return \Drupal\data_transfer\Export\Transformer\ExportTransformerInterface
   *   The transformer.
   */
  public function createFromSpecification(
    PluginSpecificationInterface $specification,
    array $input = []
  );

}
