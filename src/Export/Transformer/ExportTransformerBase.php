<?php

namespace Drupal\data_transfer\Export\Transformer;

use Drupal\data_transfer\Plugin\PluginBase;

/**
 * Provides base class for export data transformers.
 */
abstract class ExportTransformerBase extends PluginBase implements ExportTransformerInterface {

  /**
   * The source path setting.
   */
  public const SOURCE_PATH_SETTING = 'from';

  /**
   * Returns source path parsed to an array.
   *
   * The path contains record keys to use. Last key contains the source entity,
   * the keys before it (if any) are arrays to loop over.
   *
   * @return string[]
   *   The source path.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function getSourcePath(): array {
    $source_path = $this->getSetting(static::SOURCE_PATH_SETTING);
    return explode('.*.', $source_path);
  }

  /**
   * Allows to iterate over flat sets of source records.
   *
   * @param string[] $path
   *   The source entity path.
   * @param array $data
   *   The source data to iterate over.
   * @param array $parent_path
   *   The parent path already passed by parent calls.
   *
   * @return \Generator
   *   The value is an array of:
   *   - records array passed by reference.
   *   - parent path of the record set, may be used to build full key of the
   *     record.
   *   - source entity key in the records to read the source entity from.
   */
  protected function iterateOverSourceRecordSets(
    array $path,
    array &$data,
    array $parent_path = []
  ) {
    $key = array_shift($path);
    if (empty($path)) {
      yield [&$data, $parent_path, $key];
    }
    else {
      foreach ($data as $record_key => &$record) {
        if (!isset($record[$key])) {
          continue;
        }

        $children_path = $parent_path;
        $children_path[] = $record_key;
        $children_path[] = $key;

        yield from $this->iterateOverSourceRecordSets(
          $path,
          $record[$key],
          $children_path
        );
      }
    }
  }

}
