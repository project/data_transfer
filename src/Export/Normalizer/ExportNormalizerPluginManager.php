<?php

namespace Drupal\data_transfer\Export\Normalizer;

use Drupal\data_transfer\Plugin\PluginManagerBase;

/**
 * Plugin manager of the export normalizers.
 */
class ExportNormalizerPluginManager extends PluginManagerBase implements ExportNormalizerPluginManagerInterface {

  /**
   * {@inheritdoc}
   */
  public const SUBDIR = 'Plugin/data_transfer/ExportNormalizer';

  /**
   * {@inheritdoc}
   */
  public const CACHE_KEY = 'export_normalizer';

  /**
   * {@inheritdoc}
   */
  public const PLUGIN_INTERFACE = ExportNormalizerInterface::class;

}
