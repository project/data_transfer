<?php

namespace Drupal\data_transfer\Export\Normalizer;

use Drupal\Component\Utility\NestedArray;
use Drupal\data_transfer\Exception\PluginConfigurationException;
use Drupal\data_transfer\Plugin\PluginBase;
use Drupal\data_transfer\Utility\NestedData;

/**
 * Provides base class for the export data normalizers.
 */
abstract class ExportNormalizerBase extends PluginBase implements ExportNormalizerInterface {

  /**
   * The property path delimiter.
   */
  public const PATH_DELIMITER = '.';

  /**
   * The source path setting key.
   */
  public const SOURCE_PATH_SETTING = 'from';

  /**
   * The target path setting key.
   */
  public const TARGET_PATH_SETTING = 'to';

  /**
   * The optional prefix of the source path to read value from result record.
   */
  public const TARGET_AS_SOURCE_PREFIX = '@';

  /**
   * Returns source value.
   *
   * @param array $source_record
   *   The input record to extract the value from.
   * @param array|null $result_record
   *   The result record to read the value from, if configured. If NULL is
   *   passed, the plugin doesn't support reading from the result record.
   *
   * @return mixed|null
   *   The source value or NULL if it's not set.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function getSourceValue(
    array $source_record,
    array $result_record = NULL
  ) {
    return $this->getSourceValueOf(static::SOURCE_PATH_SETTING, $source_record, $result_record);
  }

  /**
   * Returns value at the path specified by the passed plugin setting.
   *
   * @param string $key
   *   The path setting key.
   * @param array $source_record
   *   The input record to extract the value from.
   * @param array|null $result_record
   *   The result record to read the value from, if configured. If NULL is
   *   passed, the plugin doesn't support reading from the result record.
   *
   * @return mixed|null
   *   The source value or NULL if it's not set.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function getSourceValueOf(
    string $key,
    array $source_record,
    array $result_record = NULL
  ) {
    $path = $this->getSetting($key);

    return $this->getSourceValueByPath($path, $source_record, $result_record);
  }

  /**
   * Returns value at the path.
   *
   * @param string $path
   *   The path.
   * @param array $source_record
   *   The input record to extract the value from.
   * @param array|null $result_record
   *   The result record to read the value from, if configured. If NULL is
   *   passed, the plugin doesn't support reading from the result record.
   *
   * @return mixed|null
   *   The source value or NULL if it's not set.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function getSourceValueByPath(
    string $path,
    array $source_record,
    array $result_record = NULL
  ) {
    if ($path[0] === static::TARGET_AS_SOURCE_PREFIX) {
      if (!isset($result_record)) {
        throw new PluginConfigurationException(sprintf(
          "The %s plugin doesn't support reading value from the result record.",
          $this->getPluginId()
        ));
      }

      $path = substr($path, 1);
      $source = $result_record;
    }
    else {
      $source = $source_record;
    }

    $parents = explode(static::PATH_DELIMITER, $path);
    return NestedData::getValue($source, $parents);
  }

  /**
   * Sets the target value.
   *
   * @param mixed $value
   *   The value to set.
   * @param array $result
   *   The result to set to.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function setTargetValue($value, array &$result): void {
    $path = $this->getSetting(static::TARGET_PATH_SETTING);
    $parents = explode(static::PATH_DELIMITER, $path);
    NestedArray::setValue($result, $parents, $value);
  }

}
