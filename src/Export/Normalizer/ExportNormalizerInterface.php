<?php

namespace Drupal\data_transfer\Export\Normalizer;

/**
 * The export data normalizer.
 *
 * The purpose of the normalizer is to extract necessary data from the source
 * record and put it to the result record as the most primitive value accepted
 * by the writer.
 */
interface ExportNormalizerInterface {

  /**
   * Normalizes the data according to configuration.
   *
   * Usually it takes a single property from the source, formats it and puts to
   * the result, but it's not really restricted to this case.
   *
   * @param array $record
   *   The record to normalize.
   * @param array $result
   *   The result to put the normalized value to.
   */
  public function normalize(array $record, array &$result): void;

}
