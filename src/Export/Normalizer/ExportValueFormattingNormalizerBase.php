<?php

namespace Drupal\data_transfer\Export\Normalizer;

/**
 * Base class for normalizers that just format the value.
 */
abstract class ExportValueFormattingNormalizerBase extends ExportNormalizerBase {

  /**
   * Formats the value.
   *
   * NULL is not passed to this method.
   *
   * @param mixed $value
   *   The value to format.
   *
   * @return mixed
   *   The formatted value.
   */
  abstract protected function formatValue($value);

  /**
   * {@inheritdoc}
   */
  public function normalize(array $record, array &$result): void {
    $value = $this->getSourceValue($record);

    if (isset($value)) {
      $value = $this->formatValue($value);
    }

    $this->setTargetValue($value, $result);
  }

}
