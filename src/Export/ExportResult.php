<?php

namespace Drupal\data_transfer\Export;

use Drupal\Core\Url;

/**
 * Represents result of a single export.
 */
class ExportResult implements ExportResultInterface {

  /**
   * The key of the storage that holds the number of exported records.
   */
  public const EXPORTED_KEY = 'exported';

  /**
   * The key of the storage that holds the result URL, if any.
   */
  public const URL_KEY = 'url';

  /**
   * The key of the storage that holds the number of total records to export.
   */
  public const TOTAL_KEY = 'total';

  /**
   * The actual data storage.
   *
   * @var array
   */
  protected $storage = [];

  /**
   * A constructor.
   *
   * @param array $storage
   *   The data storage, as returned by ::toArray() method.
   */
  public function __construct(array $storage = []) {
    $storage += [
      static::EXPORTED_KEY => 0,
      static::URL_KEY => NULL,
      static::TOTAL_KEY => 0,
    ];
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  public function exported(int $count = 1): ExportResultInterface {
    $this->storage[static::EXPORTED_KEY] += $count;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function countExported(): int {
    return $this->storage[static::EXPORTED_KEY];
  }

  /**
   * {@inheritdoc}
   */
  public function setUrl(Url $url): ExportResultInterface {
    $this->storage[static::URL_KEY] = $url->toString();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasUrl(): bool {
    return isset($this->storage[static::URL_KEY]);
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl(): string {
    return $this->storage[static::URL_KEY];
  }

  /**
   * {@inheritdoc}
   */
  public function toArray(): array {
    return $this->storage;
  }

  /**
   * {@inheritdoc}
   */
  public function setTotal(int $total): ExportResultInterface {
    $this->storage[static::TOTAL_KEY] = $total;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotal(): int {
    return $this->storage[static::TOTAL_KEY];
  }

}
