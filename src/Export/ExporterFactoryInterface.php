<?php

namespace Drupal\data_transfer\Export;

use Drupal\data_transfer\Entity\ExportSpecificationInterface;

/**
 * Provides exporter factory.
 */
interface ExporterFactoryInterface {

  /**
   * Creates instance of exporter for the passed export specification.
   *
   * @param \Drupal\data_transfer\Entity\ExportSpecificationInterface $export_specification
   *   The export specification to follow.
   * @param array $input
   *   The input array to pass to the exporter.
   *
   * @return \Drupal\data_transfer\Export\ExporterInterface
   *   The exporter instance.
   */
  public function createExporter(
    ExportSpecificationInterface $export_specification,
    array $input = []
  ): ExporterInterface;

}
