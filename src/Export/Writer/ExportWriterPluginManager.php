<?php

namespace Drupal\data_transfer\Export\Writer;

use Drupal\data_transfer\Plugin\PluginManagerBase;

/**
 * Plugin manager of the export data writers.
 */
class ExportWriterPluginManager extends PluginManagerBase implements ExportWriterPluginManagerInterface {

  /**
   * {@inheritdoc}
   */
  public const SUBDIR = 'Plugin/data_transfer/ExportWriter';

  /**
   * {@inheritdoc}
   */
  public const CACHE_KEY = 'export_writer';

  /**
   * {@inheritdoc}
   */
  public const PLUGIN_INTERFACE = ExportWriterInterface::class;

}
