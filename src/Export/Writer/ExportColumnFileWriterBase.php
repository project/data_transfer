<?php

namespace Drupal\data_transfer\Export\Writer;

use Drupal\data_transfer\Export\ExportResultInterface;
use Drupal\data_transfer\Export\Writer\Rows\LineBuilderFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for the file writer that contains data columns.
 */
abstract class ExportColumnFileWriterBase extends ExportFileWriterBase {

  /**
   * The plugin setting key of the column mapping.
   */
  public const COLUMNS_SETTING = 'columns';

  /**
   * The column mapping key the line builder expects on the line config.
   */
  protected const LINE_COLUMNS_SETTING = LineBuilderFactory::COLUMNS_SETTING;

  /**
   * The line builder factory.
   *
   * @var \Drupal\data_transfer\Export\Writer\Rows\LineBuilderFactoryInterface
   */
  protected $lineBuilderFactory;

  /**
   * The cached list of line builders.
   *
   * @var \Drupal\data_transfer\Export\Writer\Rows\LineBuilderInterface[]
   */
  protected $lineBuilders;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->lineBuilderFactory = $container->get(
      'data_transfer.export.writer.line_builder_factory'
    );

    return $instance;
  }

  /**
   * Writes column values to the file.
   *
   * @param array $column_values
   *   The column values of a single line to write.
   */
  abstract protected function writeColumnValues(array $column_values): void;

  /**
   * Creates line builders to be used for every record of the normalized data.
   *
   * @return \Drupal\data_transfer\Export\Writer\Rows\LineBuilderInterface[]
   *   The list of line builders.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function createLineBuilders(): array {
    $columns_config = $this->getSetting(static::COLUMNS_SETTING);
    $lines_config = [
      [static::LINE_COLUMNS_SETTING => $columns_config],
    ];
    return $this->lineBuilderFactory->createLineBuilders($lines_config);
  }

  /**
   * Returns line builders to be used for every record of the normalized data.
   *
   * @return \Drupal\data_transfer\Export\Writer\Rows\LineBuilderInterface[]
   *   The list of line builders.
   */
  protected function getLineBuilders(): array {
    if (!isset($this->lineBuilders)) {
      $this->lineBuilders = $this->createLineBuilders();
    }

    return $this->lineBuilders;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   * @throws \Drupal\data_transfer\Exception\ExchangeException
   * @throws \Drupal\data_transfer\Exception\ExchangeFileException
   */
  public function write(array $records, ExportResultInterface $result): void {
    $line_builders = $this->getLineBuilders();

    foreach ($records as $record) {
      foreach ($line_builders as $line_builder) {
        $lines = $line_builder->getLines($record);
        foreach ($lines as $line) {
          $this->writeColumnValues($line);
        }
      }
      $result->exported();
    }
  }

}
