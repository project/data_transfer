<?php

namespace Drupal\data_transfer\Export\Writer;

use Drupal\Core\File\Exception\DirectoryNotReadyException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\data_transfer\Exception\ExchangeException;
use Drupal\data_transfer\Exception\ExchangeFileException;
use Drupal\data_transfer\Exception\PluginConfigurationException;
use Drupal\data_transfer\Plugin\PluginBase;
use Drupal\data_transfer\Plugin\PluginWithStorageInterface;
use Drupal\data_transfer\Plugin\PluginWithStorageTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base class for file-aware export writers.
 *
 * It only implements some useful methods to generate temporary & target
 * filepaths and to build the result file URL. It doesn't open/close the files
 * and doesn't make any assumptions on the file workflow, so child classes could
 * be more flexible.
 *
 * @see \Drupal\data_transfer\Export\Writer\ExportFileWriterBase
 */
abstract class ExportFileAwareWriterBase extends PluginBase implements ExportWriterInterface, PluginWithStorageInterface, ContainerFactoryPluginInterface {

  use PluginWithStorageTrait;

  /**
   * The plugin storage key of the file path.
   */
  public const TEMPORARY_FILE_PATH_KEY = 'temp_file_path';

  /**
   * The directory to create the temporary file.
   */
  public const TEMPORARY_DIRECTORY = 'temporary://';

  /**
   * The temporary file name prefix.
   */
  public const TEMPORARY_FILE_PREFIX = 'export';

  /**
   * The settings key of the file name pattern.
   */
  public const FILE_NAME_PATTERN_SETTING = 'file_name';

  /**
   * The settings key of the target folder, with schema.
   */
  public const FOLDER_SETTING = 'folder';

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The target file name.
   *
   * @var string
   */
  protected $targetFileName;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->fileSystem = $container->get('file_system');

    return $instance;
  }

  /**
   * Creates the target file name from the pattern.
   *
   * @return string
   *   The file name.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function createTargetFileName(): string {
    if (empty($this->targetFileName)) {
      $pattern = $this->configuration[static::FILE_NAME_PATTERN_SETTING] ?? NULL;
      if (!isset($pattern)) {
        throw new PluginConfigurationException('File name pattern is required.');
      }

      $replacements = $this->getFileNamePlaceholders();
      $this->targetFileName = strtr($pattern, $replacements);
    }

    return $this->targetFileName;
  }

  /**
   * Returns the list of placeholders and corresponding replacements.
   *
   * These placeholders get replaced in the target file name.
   *
   * @return array
   *   Keys are placeholders, values are replacements.
   */
  abstract protected function getFileNamePlaceholders(): array;

  /**
   * Prepares the target folder.
   *
   * @return string
   *   The folder URI.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   * @throws \Drupal\Core\File\Exception\DirectoryNotReadyException
   */
  protected function prepareTargetFolder(): string {
    $folder = $this->configuration[static::FOLDER_SETTING] ?? NULL;
    if (!isset($folder)) {
      throw new PluginConfigurationException('Folder is required.');
    }

    $is_ready = $this->fileSystem->prepareDirectory(
      $folder,
      FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS
    );
    if (!$is_ready) {
      throw new DirectoryNotReadyException(sprintf(
        "The '%s' directory is not writable or doesn't exist.",
        $folder
      ));
    }

    return $folder;
  }

  /**
   * Creates the target file URI.
   *
   * @return string
   *   The target file URI.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   * @throws \Drupal\Core\File\Exception\DirectoryNotReadyException
   */
  protected function createTargetFilePath(): string {
    return implode('/', [
      $this->prepareTargetFolder(),
      $this->createTargetFileName(),
    ]);
  }

  /**
   * Creates file path of the temporary file.
   *
   * @return string
   *   The temporary filepath.
   *
   * @throws \Drupal\data_transfer\Exception\ExchangeFileException
   */
  protected function createTemporaryFilePath(): string {
    $directory = static::TEMPORARY_DIRECTORY;

    $result = $this->fileSystem->tempnam(
      $directory,
      static::TEMPORARY_FILE_PREFIX
    );
    if ($result === FALSE) {
      throw new ExchangeFileException(sprintf(
        'Unable to create temporary file in %s directory.',
        $directory
      ));
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   * @throws \Drupal\Core\File\Exception\DirectoryNotReadyException
   */
  protected function getTemporaryFilePath(): string {
    $storage = $this->getStorage();
    $key = static::TEMPORARY_FILE_PATH_KEY;

    $filepath = $storage->get($key);
    if (!isset($filepath)) {
      $filepath = $this->createTemporaryFilePath();
      $storage->set($key, $filepath);
    }

    return $filepath;
  }

  /**
   * Builds the result download URL.
   *
   * @return \Drupal\Core\Url
   *   The URL.
   *
   * @throws \Drupal\data_transfer\Exception\ExchangeException
   */
  protected function buildResultUrl(string $filepath): Url {
    $url_string = file_create_url($filepath);
    if ($url_string === FALSE) {
      throw new ExchangeException(sprintf(
        'Unable to create public URL for file: %s',
        $filepath
      ));
    }

    return Url::fromUri($url_string);
  }

}
