<?php

namespace Drupal\data_transfer\Export\Writer\Columns;

/**
 * Returns static cell value specified in the config.
 */
class CellStaticBuilder implements CellBuilderInterface {

  /**
   * The column configuration key that triggers this cell builder.
   */
  public const CONFIG_KEY = 'value';

  /**
   * The static value to return.
   *
   * @var mixed
   */
  protected $value;

  /**
   * A constructor.
   *
   * @param array $column_config
   *   The column configuration.
   */
  public function __construct(array $column_config) {
    $this->value = $column_config[static::CONFIG_KEY];
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(array $record) {
    return $this->value;
  }

}
