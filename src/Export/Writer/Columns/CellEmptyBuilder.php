<?php

namespace Drupal\data_transfer\Export\Writer\Columns;

/**
 * Produces empty cell.
 *
 * @see \Drupal\data_transfer\Export\Writer\Columns\CellBuilderInterface
 */
class CellEmptyBuilder implements CellBuilderInterface {

  /**
   * The column configuration key that triggers this cell builder.
   */
  public const CONFIG_KEY = 'empty';

  /**
   * The value to return.
   */
  public const EMPTY_VALUE = '';

  /**
   * {@inheritdoc}
   */
  public function getValue(array $record) {
    return static::EMPTY_VALUE;
  }

}
