<?php

namespace Drupal\data_transfer\Export\Writer\Columns;

/**
 * Returns cell value from a specified key in the normalized data record.
 */
class CellSourceBuilder implements CellBuilderInterface {

  /**
   * The column configuration key that triggers this cell builder.
   *
   * Its value must be a key in the normalized data to get the value from.
   */
  public const CONFIG_KEY = 'source';

  /**
   * The fallback value to use when no data found in the normalized record.
   */
  public const FALLBACK_VALUE = '';

  /**
   * The key in the normalized record to get the cell value from.
   *
   * @var string
   */
  protected $sourceKey;

  /**
   * A constructor.
   */
  public function __construct(array $column_config) {
    $this->sourceKey = $column_config[static::CONFIG_KEY];
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(array $record) {
    return $record[$this->sourceKey] ?? static::FALLBACK_VALUE;
  }

}
