<?php

namespace Drupal\data_transfer\Export\Writer\Columns;

use Drupal\data_transfer\Exception\PluginConfigurationException;

/**
 * Creates cell builders from a list of column configurations.
 */
class CellBuilderFactory implements CellBuilderFactoryInterface {

  /**
   * Maps column config key to a corresponding cell builder class.
   */
  public const PROPERTY_READER_CLASS_MAP = [
    CellSourceBuilder::CONFIG_KEY => CellSourceBuilder::class,
    CellStaticBuilder::CONFIG_KEY  => CellStaticBuilder::class,
    CellEmptyBuilder::CONFIG_KEY  => CellEmptyBuilder::class,
  ];

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  public function createCellBuilders(array $columns_config): array {
    $cell_builders = [];

    foreach ($columns_config as $index => $column_config) {
      $matching_classes = array_intersect_key(
        static::PROPERTY_READER_CLASS_MAP,
        $column_config
      );
      if (count($matching_classes) !== 1) {
        throw new PluginConfigurationException(sprintf(
          'The %s column mapping must have exactly one of the following keys: %s',
          $index,
          implode(', ', array_keys(static::PROPERTY_READER_CLASS_MAP))
        ));
      }

      $class_name = reset($matching_classes);
      $cell_builders[$index] = new $class_name($column_config);
    }

    return $cell_builders;
  }

}
