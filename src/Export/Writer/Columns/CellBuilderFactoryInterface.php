<?php

namespace Drupal\data_transfer\Export\Writer\Columns;

/**
 * Creates cell builders from a list of column configurations.
 */
interface CellBuilderFactoryInterface {

  /**
   * Creates a list of cell builders from a list of column configurations.
   *
   * @param array $columns_config
   *   The list of column configurations.
   *
   * @return \Drupal\data_transfer\Export\Writer\Columns\CellBuilderInterface[]
   *   The cell builders. Keys from the configuration list are preserved.
   */
  public function createCellBuilders(array $columns_config): array;

}
