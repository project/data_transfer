<?php

namespace Drupal\data_transfer\Export\Writer\Columns;

/**
 * Interface of a cell builder for a table-like files.
 *
 * Its purpose is to provide the same interface for all kinds of cells without
 * any conditions in the caller code. For example, child class could read cell
 * value from the normalized data record, provide static or empty value.
 */
interface CellBuilderInterface {

  /**
   * Returns cell value to write to the file.
   *
   * @param array $record
   *   The normalized data record.
   *
   * @return mixed
   *   The value of the cell to write.
   */
  public function getValue(array $record);

}
