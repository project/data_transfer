<?php

namespace Drupal\data_transfer\Export\Writer;

use Drupal\Core\File\FileSystemInterface;
use Drupal\data_transfer\Exception\ExchangeFileException;
use Drupal\data_transfer\Export\ExportResultInterface;

/**
 * Provides base class for file writers.
 *
 * It makes some assumptions on the file workflow. See file-aware writer base
 * class in case you need more flexibility.
 *
 * @see \Drupal\data_transfer\Export\Writer\ExportFileAwareWriterBase
 */
abstract class ExportFileWriterBase extends ExportFileAwareWriterBase {

  /**
   * File open mode for initialization.
   */
  public const INIT_FILE_MODE = 'wb';

  /**
   * File open mode for existing file.
   */
  public const OPEN_FILE_MODE = 'ab';

  /**
   * The flags to use when moving temporary file to the target path.
   */
  protected const TEMPORARY_FILE_MOVE_FLAGS = FileSystemInterface::EXISTS_RENAME;

  /**
   * The file resource.
   *
   * @var resource
   */
  protected $file;

  /**
   * Opens the file in the specified mode.
   *
   * @param string $mode
   *   The mode to use.
   *
   * @throws \LogicException
   * @throws \Drupal\data_transfer\Exception\ExchangeFileException
   */
  protected function openFile(string $mode): void {
    if (isset($this->file)) {
      throw new \LogicException('The file is already open.');
    }

    $filepath = $this->getTemporaryFilePath();
    $file = fopen($filepath, $mode);
    if ($file === FALSE) {
      $this->cleanupFile();
      throw new ExchangeFileException(sprintf(
        'Unable to open the file in %s mode: %s',
        $mode,
        $filepath
      ));
    }

    $this->file = $file;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \LogicException
   * @throws \Drupal\data_transfer\Exception\ExchangeFileException
   */
  public function init(): void {
    $this->openFile(static::INIT_FILE_MODE);
    $this->writeFileHeader();
    $this->closeFile();
  }

  /**
   * Writes header to the file.
   */
  protected function writeFileHeader(): void {
    // Does nothing by default.
  }

  /**
   * {@inheritdoc}
   *
   * @throws \LogicException
   * @throws \Drupal\data_transfer\Exception\ExchangeFileException
   */
  public function open(): void {
    $this->openFile(static::OPEN_FILE_MODE);
  }

  /**
   * Closes the file.
   *
   * @throws \Drupal\data_transfer\Exception\ExchangeFileException
   */
  protected function closeFile(): void {
    if (!isset($this->file)) {
      return;
    }

    $closed = fclose($this->file);
    unset($this->file);
    if ($closed === FALSE) {
      $this->cleanupFile();
      throw new ExchangeFileException(sprintf(
        'Unable to close the export file: %s',
        $this->getTemporaryFilePath()
      ));
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\data_transfer\Exception\ExchangeFileException
   */
  public function close(): void {
    $this->closeFile();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\data_transfer\Exception\ExchangeException
   * @throws \Drupal\data_transfer\Exception\ExchangeFileException
   */
  public function finish(ExportResultInterface $result): void {
    $file_path = $this->moveTemporaryToTargetFile();
    $this->saveManagedFile($file_path);
    $url = $this->buildResultUrl($file_path);
    $result->setUrl($url);
  }

  /**
   * Cleanups the export file after operation failure.
   *
   * @throws \Drupal\data_transfer\Exception\ExchangeFileException
   */
  protected function cleanupFile(): void {
    $this->closeFile();

    $filepath = $this->getTemporaryFilePath();
    if (file_exists($filepath)) {
      $this->fileSystem->unlink($filepath);
    }
  }

  /**
   * Moves temporary file to the target location.
   *
   * @return string
   *   The actual target filepath.
   *
   * @throws \Drupal\data_transfer\Exception\ExchangeFileException
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function moveTemporaryToTargetFile(): string {
    $temporary_filepath = $this->getTemporaryFilePath();
    $target_filepath = $this->createTargetFilePath();

    try {
      $result = $this->fileSystem->move(
        $temporary_filepath,
        $target_filepath,
        static::TEMPORARY_FILE_MOVE_FLAGS
      );
    }
    catch (\Throwable $e) {
      $this->cleanupFile();
      throw $e;
    }

    return $result;
  }

  /**
   * Saves the managed file, if necessary.
   *
   * The method is called after temporary file has been moved to the target
   * location.
   *
   * @param string $filepath
   *   The actual target filepath.
   */
  protected function saveManagedFile(string $filepath): void {
    // Do nothing by default.
  }

}
