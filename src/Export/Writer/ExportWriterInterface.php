<?php

namespace Drupal\data_transfer\Export\Writer;

use Drupal\data_transfer\Export\ExportResultInterface;

/**
 * The export data writer.
 *
 * The purpose of the writer is to write the normalized data into the
 * destination, e.g. file, API, database.
 */
interface ExportWriterInterface {

  /**
   * Initializes the output storage.
   */
  public function init(): void;

  /**
   * Opens an already initialized storage for appending the data.
   */
  public function open(): void;

  /**
   * Writes a set of the normalized data records.
   *
   * @param array $records
   *   The normalized records.
   * @param \Drupal\data_transfer\Export\ExportResultInterface $result
   *   The result to populate with statistics.
   */
  public function write(array $records, ExportResultInterface $result): void;

  /**
   * Closes the storage after writing.
   */
  public function close(): void;

  /**
   * Finishes the storage, if necessary and populates the result.
   *
   * @param \Drupal\data_transfer\Export\ExportResultInterface $result
   *   The result to populate with result URL (if any) and statistics.
   */
  public function finish(ExportResultInterface $result): void;

}
