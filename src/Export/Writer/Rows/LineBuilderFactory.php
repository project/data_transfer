<?php

namespace Drupal\data_transfer\Export\Writer\Rows;

use Drupal\data_transfer\Exception\PluginConfigurationException;
use Drupal\data_transfer\Export\Writer\Columns\CellBuilderFactoryInterface;

/**
 * Creates line builders from a list of line configurations.
 */
class LineBuilderFactory implements LineBuilderFactoryInterface {

  /**
   * The line configuration key that contains column configurations.
   */
  public const COLUMNS_SETTING = 'columns';

  /**
   * The line builder class to use for loop line configuration.
   */
  public const LOOP_LINE_BUILDER = LoopLineBuilder::class;

  /**
   * The line builder class to use for simple line configuration.
   */
  public const SINGLE_LINE_BUILDER = SingleLineBuilder::class;

  /**
   * The line configuration key that holds loop property.
   */
  public const LOOP_PROPERTY_SETTING = LoopLineBuilder::LOOP_PROPERTY_SETTING;

  /**
   * The cell builder factory.
   *
   * @var \Drupal\data_transfer\Export\Writer\Columns\CellBuilderFactoryInterface
   */
  protected $cellBuilderFactory;

  /**
   * A constructor.
   *
   * @param \Drupal\data_transfer\Export\Writer\Columns\CellBuilderFactoryInterface $cell_builder_factory
   *   The cell builder factory.
   */
  public function __construct(
    CellBuilderFactoryInterface $cell_builder_factory
  ) {
    $this->cellBuilderFactory = $cell_builder_factory;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  public function createLineBuilders(array $lines_config): array {
    $line_builders = [];

    foreach ($lines_config as $line_key => $line_config) {
      $columns_config = $line_config[static::COLUMNS_SETTING] ?? NULL;
      if (empty($columns_config)) {
        throw new PluginConfigurationException(
          'Columns configuration is required on every line.'
        );
      }

      $cell_builders = $this->cellBuilderFactory
        ->createCellBuilders($columns_config);

      $loop_property = $line_config[static::LOOP_PROPERTY_SETTING] ?? NULL;
      if (isset($loop_property)) {
        $class_name = static::LOOP_LINE_BUILDER;
      }
      else {
        $class_name = static::SINGLE_LINE_BUILDER;
      }

      $line_builders[$line_key] = new $class_name($cell_builders, $line_config);
    }

    return $line_builders;
  }

}
