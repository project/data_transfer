<?php

namespace Drupal\data_transfer\Export\Writer\Rows;

/**
 * Interface of a line builder for a table-like files.
 *
 * Its purpose is to provide the same interface for all kinds of line builders
 * without any conditions in the caller code. For example, child class could
 * produce multiple lines from one record or check some conditions to decide if
 * the line should be produced for a record, etc.
 */
interface LineBuilderInterface {

  /**
   * Returns a set of lines to be written to the file.
   *
   * @param array $record
   *   The normalized data record.
   *
   * @return array
   *   The list of data lines, every line contains column data.
   */
  public function getLines(array $record): array;

}
