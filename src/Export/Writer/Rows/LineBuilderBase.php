<?php

namespace Drupal\data_transfer\Export\Writer\Rows;

/**
 * Base class of a line builder for table-like files.
 */
abstract class LineBuilderBase implements LineBuilderInterface {

  /**
   * The list of cell builders.
   *
   * @var \Drupal\data_transfer\Export\Writer\Columns\CellBuilderInterface[]
   */
  protected $cellBuilders;

  /**
   * A constructor.
   *
   * @param \Drupal\data_transfer\Export\Writer\Columns\CellBuilderInterface[] $cell_builders
   *   The list of cell builders to use for the line contents.
   */
  public function __construct(array $cell_builders) {
    $this->cellBuilders = $cell_builders;
  }

}
