<?php

namespace Drupal\data_transfer\Export\Writer\Rows;

/**
 * Builds a single line from the data record.
 */
class SingleLineBuilder extends LineBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function getLines(array $record): array {
    $line = [];
    foreach ($this->cellBuilders as $column_key => $builder) {
      $line[$column_key] = $builder->getValue($record);
    }
    return [$line];
  }

}
