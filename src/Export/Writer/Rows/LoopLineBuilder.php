<?php

namespace Drupal\data_transfer\Export\Writer\Rows;

use Drupal\data_transfer\Exception\PluginConfigurationException;

/**
 * Builds a set of lines from an array property in the data record.
 *
 * The following keys are supported on the line configuration:
 * - foreach: (required) the property name to read the array of sub-records
 *   from. Each sub-record produces a single line.
 */
class LoopLineBuilder extends LineBuilderBase {

  /**
   * The setting key that contains property to read the sub-records from.
   */
  public const LOOP_PROPERTY_SETTING = 'foreach';

  /**
   * The property name in the normalized data to read the sub-records from.
   *
   * @var string
   */
  protected $sourceKey;

  /**
   * A constructor.
   *
   * @param array $cell_builders
   *   The list of cell builders to use when building the line data.
   * @param array $line_config
   *   The line configuration.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  public function __construct(array $cell_builders, array $line_config = []) {
    $this->sourceKey = $line_config[static::LOOP_PROPERTY_SETTING] ?? NULL;
    if (!isset($this->sourceKey)) {
      throw new PluginConfigurationException(sprintf(
        'Loop line builder requires %s config key.',
        static::LOOP_PROPERTY_SETTING
      ));
    }

    parent::__construct($cell_builders);
  }

  /**
   * {@inheritdoc}
   */
  public function getLines(array $record): array {
    $lines = [];

    $subrecords = $record[$this->sourceKey] ?? [];
    foreach ($subrecords as $subrecord) {
      $line = [];
      foreach ($this->cellBuilders as $column_key => $builder) {
        $line[$column_key] = $builder->getValue($subrecord);
      }
      $lines[] = $line;
    }

    return $lines;
  }

}
