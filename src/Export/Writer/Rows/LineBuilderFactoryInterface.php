<?php

namespace Drupal\data_transfer\Export\Writer\Rows;

/**
 * Creates line builders from a list of line configurations.
 */
interface LineBuilderFactoryInterface {

  /**
   * Creates a list of line builders from a list of line configurations.
   *
   * @param array $lines_config
   *   The list of line configurations.
   *
   * @return \Drupal\data_transfer\Export\Writer\Rows\LineBuilderInterface[]
   *   The list of line builders. Keys from the configs are preserved.
   */
  public function createLineBuilders(array $lines_config): array;

}
