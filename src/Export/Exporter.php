<?php

namespace Drupal\data_transfer\Export;

use Drupal\Core\Lock\LockBackendInterface;
use Drupal\data_transfer\Entity\ExportSpecificationInterface;
use Drupal\data_transfer\Exception\ParallelExecutionException;
use Drupal\data_transfer\Export\Loader\ExportLoaderPluginManagerInterface;
use Drupal\data_transfer\Export\Normalizer\ExportNormalizerPluginManagerInterface;
use Drupal\data_transfer\Export\Transformer\ExportTransformerPluginManagerInterface;
use Drupal\data_transfer\Export\Writer\ExportWriterPluginManagerInterface;

/**
 * Provides simple exporter that does the export without any optimizations.
 *
 * @see \Drupal\data_transfer\Export\ExporterFactory
 */
class Exporter extends ExporterBase implements ExporterInterface {

  /**
   * The lock ID prefix.
   */
  public const LOCK_PREFIX = 'data_transfer_exporter:';

  /**
   * The lock backend.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * A constructor.
   *
   * @param \Drupal\data_transfer\Entity\ExportSpecificationInterface $export_specification
   *   The export specification to follow.
   * @param array $input
   *   The input data passed to all the loader/normalizer/writer plugins.
   * @param \Drupal\data_transfer\Export\Loader\ExportLoaderPluginManagerInterface $loader_manager
   *   The loader plugin manager.
   * @param \Drupal\data_transfer\Export\Transformer\ExportTransformerPluginManagerInterface $transformer_manager
   *   The transformer plugin manager.
   * @param \Drupal\data_transfer\Export\Normalizer\ExportNormalizerPluginManagerInterface $normalizer_manager
   *   The normalizer plugin manager.
   * @param \Drupal\data_transfer\Export\Writer\ExportWriterPluginManagerInterface $writer_manager
   *   The writer plugin manager.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock backend.
   */
  public function __construct(
    ExportSpecificationInterface $export_specification,
    array $input,
    ExportLoaderPluginManagerInterface $loader_manager,
    ExportTransformerPluginManagerInterface $transformer_manager,
    ExportNormalizerPluginManagerInterface $normalizer_manager,
    ExportWriterPluginManagerInterface $writer_manager,
    LockBackendInterface $lock
  ) {
    parent::__construct(
      $export_specification,
      $input,
      $loader_manager,
      $transformer_manager,
      $normalizer_manager,
      $writer_manager
    );

    $this->lock = $lock;
  }

  /**
   * Reads the data using loader plugin.
   *
   * @return array
   *   The data. First level keys are indexes, second level keys are data keys
   *   and values are entities (or any other kind of data).
   */
  protected function readData(): array {
    $loader = $this->getLoader();
    $ids = $loader->loadIds();
    return $loader->loadData($ids);
  }

  /**
   * Writes normalized data into the destination location.
   *
   * @param array $records
   *   The normalized data.
   * @param \Drupal\data_transfer\Export\ExportResultInterface $result
   *   The result to populate.
   */
  protected function writeData(
    array $records,
    ExportResultInterface $result
  ): void {
    $writer = $this->getWriter();
    $writer->init();
    $writer->open();
    $writer->write($records, $result);
    $writer->close();
    $writer->finish($result);
  }

  /**
   * Returns the lock ID.
   *
   * @return string
   *   The lock ID.
   */
  protected function getLockId(): string {
    return static::LOCK_PREFIX . $this->exportSpecification->id();
  }

  /**
   * Performs the export.
   *
   * @return \Drupal\data_transfer\Export\ExportResultInterface
   *   The export result.
   */
  protected function doExport(): ExportResultInterface {
    $result = $this->createExportResult();

    $data = $this->readData();
    $data = $this->transformData($data);
    $data = $this->normalizeData($data);
    $this->writeData($data, $result);

    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\data_transfer\Exception\ParallelExecutionException
   */
  public function export(): ExportResultInterface {
    $lock_id = $this->getLockId();
    if (!$this->lock->acquire($lock_id)) {
      throw new ParallelExecutionException();
    }

    try {
      return $this->doExport();
    }
    finally {
      $this->lock->release($lock_id);
    }
  }

}
