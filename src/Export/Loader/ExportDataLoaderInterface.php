<?php

namespace Drupal\data_transfer\Export\Loader;

/**
 * The export data loader.
 *
 * The purpose of the loader is to list keys/IDs of all the data available for
 * export and then load full entities/data for the normalizers to extract the
 * values from.
 */
interface ExportDataLoaderInterface {

  /**
   * Returns total number of records that may be exported.
   *
   * @return int
   *   The number of records.
   */
  public function getTotalCount(): int;

  /**
   * Returns keys/IDs of the data to be exported.
   *
   * These IDs are then passed to the loadData() method.
   *
   * @param int $offset
   *   The offset to use for ID selection.
   * @param int $limit
   *   The maximum amount of IDs to return.
   *
   * @return array
   *   The keys/IDs.
   */
  public function loadIds(int $offset = NULL, int $limit = NULL): array;

  /**
   * Loads data for a subset of the keys/IDs.
   *
   * @param array $ids
   *   The keys/IDs returned by the loadIds() method.
   *
   * @return array
   *   The array of records for normalization. It's a good idea to wrap the main
   *   entity into an associative array, as later some related entities may be
   *   added to the record.
   */
  public function loadData(array $ids): array;

}
