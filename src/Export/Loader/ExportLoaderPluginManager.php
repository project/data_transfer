<?php

namespace Drupal\data_transfer\Export\Loader;

use Drupal\data_transfer\Plugin\PluginManagerBase;

/**
 * Plugin manager of the export data loaders.
 */
class ExportLoaderPluginManager extends PluginManagerBase implements ExportLoaderPluginManagerInterface {

  /**
   * {@inheritdoc}
   */
  public const SUBDIR = 'Plugin/data_transfer/ExportLoader';

  /**
   * {@inheritdoc}
   */
  public const CACHE_KEY = 'export_loader';

  /**
   * {@inheritdoc}
   */
  public const PLUGIN_INTERFACE = ExportDataLoaderInterface::class;

}
