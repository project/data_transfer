<?php

namespace Drupal\data_transfer\Export\Loader;

use Drupal\data_transfer\Exception\PluginConfigurationException;
use Drupal\data_transfer\Plugin\PluginBase;
use Drupal\data_transfer\Plugin\views\display\ExportSourceDisplay;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;

/**
 * Provides base class for export loaders that retrieve source data from view.
 */
abstract class ViewsExportLoaderBase extends PluginBase implements ExportDataLoaderInterface {

  /**
   * The setting key of the view ID.
   */
  public const VIEW_ID_SETTING = 'view';

  /**
   * The setting key of the view display ID.
   */
  public const DISPLAY_ID_SETTING = 'display';

  /**
   * The filters key in the input array.
   */
  public const FILTERS_INPUT_KEY = 'filters';

  /**
   * The export options name used by the views display.
   */
  protected const EXPORT_OPTIONS_KEY = ExportSourceDisplay::EXPORT_OPTIONS_KEY;

  /**
   * The limit key in the views display options.
   */
  protected const LIMIT_OPTION_KEY = ExportSourceDisplay::LIMIT_OPTION_KEY;

  /**
   * The key in the views display options that enables the total counting.
   */
  protected const GET_TOTAL_OPTION_KEY = ExportSourceDisplay::GET_TOTAL_OPTION_KEY;

  /**
   * The offset key in the views display options.
   */
  protected const OFFSET_OPTION_KEY = ExportSourceDisplay::OFFSET_OPTION_KEY;

  /**
   * Returns the view executable.
   *
   * @return \Drupal\views\ViewExecutable
   *   The view executable.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function getViewExecutable(): ViewExecutable {
    $view_id = $this->getSetting(static::VIEW_ID_SETTING);
    $display_id = $this->getSetting(static::DISPLAY_ID_SETTING);

    $view = Views::getView($view_id);
    if (!$view instanceof ViewExecutable) {
      throw new PluginConfigurationException(sprintf(
        "The %s view doesn't exist.",
        $view_id
      ));
    }

    if (!$view->setDisplay($display_id)) {
      throw new PluginConfigurationException(sprintf(
        "The %s display of the %s view doesn't exist.",
        $view_id,
        $display_id
      ));
    }

    $filters = $this->getInputValue(static::FILTERS_INPUT_KEY) ?? [];
    $view->setExposedInput($filters);

    return $view;
  }

  /**
   * Passes the options to the views display handler.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view.
   * @param array $options
   *   The options to pass.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function setViewDisplayOptions(
    ViewExecutable $view,
    array $options
  ) {
    $display_id = $this->getSetting(static::DISPLAY_ID_SETTING);
    $view->displayHandlers->get($display_id)
      ->setOption(static::EXPORT_OPTIONS_KEY, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalCount(): int {
    $view = $this->getViewExecutable();

    // @todo Find a way to retrieve the total number of results without
    //   executing the main query. For now we get 1 result.
    $options = [
      static::OFFSET_OPTION_KEY => 0,
      static::LIMIT_OPTION_KEY => 1,
      static::GET_TOTAL_OPTION_KEY => TRUE,
    ];
    $this->setViewDisplayOptions($view, $options);

    $result = $view->preview();
    return $result['#total'] ?? 0;
  }

  /**
   * {@inheritdoc}
   */
  public function loadIds(int $offset = NULL, int $limit = NULL): array {
    $view = $this->getViewExecutable();

    $options = [
      static::OFFSET_OPTION_KEY => $offset,
      static::LIMIT_OPTION_KEY => $limit,
      static::GET_TOTAL_OPTION_KEY => FALSE,
    ];
    $this->setViewDisplayOptions($view, $options);

    $result = $view->preview();
    return $result['#ids'] ?? [];
  }

}
