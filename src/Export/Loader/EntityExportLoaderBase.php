<?php

namespace Drupal\data_transfer\Export\Loader;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\data_transfer\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base class for the entity export loader.
 */
abstract class EntityExportLoaderBase extends PluginBase implements ExportDataLoaderInterface, ContainerFactoryPluginInterface {

  use EntityWrapperTrait,
    EntityExportLoaderTrait;

  /**
   * The entity type ID.
   *
   * Should be overridden by child class.
   */
  protected const ENTITY_TYPE = NULL;

  /**
   * The entity key in the result record.
   *
   * Should be overridden by child class.
   */
  protected const ENTITY_KEY = NULL;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * Returns the entity type definition.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   *   The entity type.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityType(): EntityTypeInterface {
    return $this->entityTypeManager
      ->getDefinition(static::ENTITY_TYPE);
  }

  /**
   * Returns the entity storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityStorage(): EntityStorageInterface {
    return $this->entityTypeManager
      ->getStorage(static::ENTITY_TYPE);
  }

  /**
   * Builds the entity query.
   *
   * May be overridden by child classes to add conditions.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The entity query.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function buildEntityQuery(): QueryInterface {
    $id_key = $this->getEntityType()
      ->getKey('id');

    $query = $this->getEntityStorage()
      ->getQuery()
      ->accessCheck(FALSE)
      ->sort($id_key);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalCount(): int {
    return $this->buildEntityQuery()
      ->count()
      ->execute();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadIds(int $offset = NULL, int $limit = NULL): array {
    $query = $this->buildEntityQuery();

    if (isset($offset) || isset($limit)) {
      $query->range($offset, $limit);
    }

    $ids_map = $query->execute();
    return array_values($ids_map);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadData(array $ids): array {
    $entities = $this->loadEntities($ids);
    return $this->wrapEntities($entities);
  }

}
