<?php

namespace Drupal\data_transfer\Export\Loader;

/**
 * Provides trait for loaders that wrap entities into associative array.
 *
 * The class must add ENTITY_KEY constant in order to specify the key of the
 * entity in the associative array or specify the key on every call to the
 * wrapEntities() method.
 */
trait EntityWrapperTrait {

  /**
   * Wraps entities with an associative array.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   The entity list.
   * @param string|null $entity_key
   *   The entity key to use for wrapping. If omitted, the ENTITY_KEY class
   *   constant is used.
   *
   * @return array
   *   The wrapped entity list.
   */
  protected function wrapEntities(
    array $entities,
    string $entity_key = NULL
  ): array {
    if ($entity_key === NULL) {
      $entity_key = static::ENTITY_KEY;
    }

    $result = [];
    foreach ($entities as $id => $entity) {
      $result[$id] = [
        $entity_key => $entity,
      ];
    }
    return $result;
  }

}
