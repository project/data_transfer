<?php

namespace Drupal\data_transfer\Export\Loader;

/**
 * Provides trait for export loaders that need entity loading.
 *
 * The loaded entity type ID could be passed to the method or specified as an
 * ENTITY_TYPE class constant.
 */
trait EntityExportLoaderTrait {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Loads the entities.
   *
   * @param array $ids
   *   The entity IDs.
   * @param string|null $entity_type_id
   *   The entity type ID. If omitted, the ENTITY_TYPE class constant is used.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The list of entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadEntities(
    array $ids,
    string $entity_type_id = NULL
  ): array {
    if (!isset($entity_type_id)) {
      $entity_type_id = static::ENTITY_TYPE;
    }

    return $this->entityTypeManager
      ->getStorage($entity_type_id)
      ->loadMultiple($ids);
  }

}
