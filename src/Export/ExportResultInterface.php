<?php

namespace Drupal\data_transfer\Export;

use Drupal\Core\Url;

/**
 * Represents result of a single export.
 */
interface ExportResultInterface {

  /**
   * Returns the number of exported records.
   *
   * @return int
   *   The number of exported records.
   */
  public function countExported(): int;

  /**
   * Increments the counter of exported records.
   *
   * @param int $count
   *   The number of records to add to the counter.
   *
   * @return static
   *   Self.
   */
  public function exported(int $count = 1): ExportResultInterface;

  /**
   * Returns the result URL.
   *
   * Make sure to check if there is an URL before calling this method.
   *
   * @return \Drupal\Core\Url
   *   The result download URL.
   */
  public function getUrl(): string;

  /**
   * Sets the result download URL.
   *
   * @param \Drupal\Core\Url $url
   *   The URL.
   *
   * @return static
   *   Self.
   */
  public function setUrl(Url $url): ExportResultInterface;

  /**
   * Checks whether the result has download URL or not.
   *
   * @return bool
   *   TRUE if result has the download URL.
   */
  public function hasUrl(): bool;

  /**
   * Builds array representation of the result.
   *
   * Result of this method could be passed to the class constructor in order to
   * re-create exactly the same result. It should be used to persist the result
   * between requests.
   *
   * @return array
   *   The array representation of the result.
   */
  public function toArray(): array;

  /**
   * Sets the total number of records.
   *
   * @param int $total
   *   The number of total records to export.
   *
   * @return static
   *   Self.
   */
  public function setTotal(int $total): ExportResultInterface;

  /**
   * Returns the number of items to export.
   *
   * @return int
   *   The number of items to export.
   */
  public function getTotal(): int;

}
