<?php

namespace Drupal\data_transfer\Export;

use Drupal\data_transfer\ArgumentsPassingFactoryBase;
use Drupal\data_transfer\Entity\ExportSpecificationInterface;

/**
 * Provides exporter factory.
 *
 * @see \Drupal\data_transfer\Export\Exporter
 */
class ExporterFactory extends ArgumentsPassingFactoryBase implements ExporterFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function createExporter(
    ExportSpecificationInterface $export_specification,
    array $input = []
  ): ExporterInterface {
    return new Exporter($export_specification, $input, ...$this->arguments);
  }

}
