<?php

namespace Drupal\data_transfer\Export\Batch;

use Drupal\data_transfer\Entity\ExportSpecificationInterface;

/**
 * Provides batch exporter factory.
 */
interface BatchExporterFactoryInterface {

  /**
   * Creates instance of batch exporter for the passed export specification.
   *
   * @param \Drupal\data_transfer\Entity\ExportSpecificationInterface $export_specification
   *   The export specification to follow.
   * @param array $input
   *   The input array to pass to the exporter.
   *
   * @return \Drupal\data_transfer\Export\Batch\BatchExporterInterface
   *   The batch exporter instance.
   */
  public function createExporter(
    ExportSpecificationInterface $export_specification,
    array $input = []
  ): BatchExporterInterface;

}
