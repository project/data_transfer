<?php

namespace Drupal\data_transfer\Export\Batch;

/**
 * Provides static callbacks to the batch exporter.
 *
 * @see \Drupal\data_transfer\Export\Batch\ExportBatchHandler
 *
 * @todo Drop this class once Drupal batch process is able to accept service
 *   methods as operation callbacks.
 */
class ExportBatchCallbacks {

  /**
   * Handles the batch operation.
   *
   * @param string $spec_id
   *   The export specification ID.
   * @param array $input
   *   The input array.
   * @param array $context
   *   The batch context.
   */
  public static function operation(
    string $spec_id,
    array $input,
    array &$context
  ): void {
    /** @var \Drupal\data_transfer\Export\Batch\ExportBatchHandlerInterface $hander */
    $hander = \Drupal::service('data_transfer.export.batch_handler');
    $hander->operation($spec_id, $input, $context);
  }

  /**
   * Batch finished callback.
   *
   * @param bool $success
   *   Indicates the success of operation.
   * @param array $results
   *   Batch process results array.
   */
  public static function finished($success, array $results): void {
    /** @var \Drupal\data_transfer\Export\Batch\ExportBatchHandlerInterface $hander */
    $hander = \Drupal::service('data_transfer.export.batch_handler');
    $hander->finished($success, $results);
  }

}
