<?php

namespace Drupal\data_transfer\Export\Batch;

use Drupal\data_transfer\ArgumentsPassingFactoryBase;
use Drupal\data_transfer\Entity\ExportSpecificationInterface;

/**
 * Provides batch exporter factory.
 *
 * @see \Drupal\data_transfer\Export\Batch\BatchExporter
 */
class BatchExporterFactory extends ArgumentsPassingFactoryBase implements BatchExporterFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function createExporter(
    ExportSpecificationInterface $export_specification,
    array $input = []
  ): BatchExporterInterface {
    return new BatchExporter(
      $export_specification,
      $input,
      ...$this->arguments
    );
  }

}
