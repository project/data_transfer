<?php

namespace Drupal\data_transfer\Export\Batch;

/**
 * Generates batch process for the data export.
 */
class ExportBatchGenerator implements ExportBatchGeneratorInterface {

  /**
   * {@inheritdoc}
   */
  public function generateBatch(string $spec_id, array $input = []): int {
    $batch_definition = [
      'progressive' => TRUE,
      'operations' => [
        [
          [ExportBatchCallbacks::class, 'operation'],
          [$spec_id, $input],
        ],
      ],
    ];
    batch_set($batch_definition);

    // Start the processing. For progressive batch it just prepares the batch
    // and assigns an ID.
    batch_process();

    $batch = batch_get();
    $batch_id = $batch['id'] ?? NULL;
    if (!isset($batch_id)) {
      throw new \LogicException('The batch ID was not assigned.');
    }

    return $batch_id;
  }

}
