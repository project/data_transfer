<?php

namespace Drupal\data_transfer\Export\Batch;

/**
 * Provides exporter for batch processing.
 */
interface BatchExporterInterface {

  /**
   * Exports a single set of records in the batch processing.
   *
   * @param array $context
   *   The batch context.
   */
  public function exportRecordSet(array &$context): void;

}
