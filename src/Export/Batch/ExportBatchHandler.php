<?php

namespace Drupal\data_transfer\Export\Batch;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\data_transfer\Entity\EntityTypes;
use Drupal\data_transfer\Entity\ExportSpecificationInterface;
use Drupal\data_transfer\Export\ExportResult;

/**
 * Handles batch export operation.
 */
class ExportBatchHandler implements ExportBatchHandlerInterface {

  use MessengerTrait;

  /**
   * The entity type ID of the export specification.
   */
  protected const EXPORT_SPEC_ENTITY_TYPE = EntityTypes::EXPORT_SPECIFICATION;

  /**
   * The key of the storage that holds the result URL, if any.
   */
  protected const URL_KEY = ExportResult::URL_KEY;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The batch exporter factory.
   *
   * @var \Drupal\data_transfer\Export\Batch\BatchExporterFactoryInterface
   */
  protected $batchExporterFactory;

  /**
   * A constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\data_transfer\Export\Batch\BatchExporterFactoryInterface $batch_exporter_factory
   *   The batch exporter factory.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    BatchExporterFactoryInterface $batch_exporter_factory
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->batchExporterFactory = $batch_exporter_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function operation(
    string $spec_id,
    array $input,
    array &$context
  ): void {
    $spec = $this->entityTypeManager
      ->getStorage(static::EXPORT_SPEC_ENTITY_TYPE)
      ->load($spec_id);
    if (!$spec instanceof ExportSpecificationInterface) {
      throw new \InvalidArgumentException(sprintf(
        "The %s export specification doesn't exist.",
        $spec_id
      ));
    }

    $exporter = $this->batchExporterFactory->createExporter($spec, $input);
    $exporter->exportRecordSet($context);
  }

  /**
   * {@inheritdoc}
   */
  public function finished($success, array $results): void {
    $messenger = $this->messenger();
    foreach ($results as $result) {
      if (!empty($result[static::URL_KEY])) {
        $messenger->addStatus($this->t(
          '<a href=":download_url">Download the result</a>.',
          [
            ':download_url' => $result[static::URL_KEY],
          ]
        ));
      }
    }
  }

}
