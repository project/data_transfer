<?php

namespace Drupal\data_transfer\Export\Batch;

/**
 * Generates batch process for the data export.
 */
interface ExportBatchGeneratorInterface {

  /**
   * Generates the batch process and returns its ID.
   *
   * It also calls batch_process() in order to generate batch ID.
   *
   * @param string $spec_id
   *   The export specification ID.
   * @param array $input
   *   The input to pass to the exporter.
   *
   * @return int
   *   The batch ID.
   */
  public function generateBatch(string $spec_id, array $input = []): int;

}
