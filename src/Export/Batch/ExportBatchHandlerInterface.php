<?php

namespace Drupal\data_transfer\Export\Batch;

/**
 * Handles batch export operation.
 */
interface ExportBatchHandlerInterface {

  /**
   * Handles the batch operation.
   *
   * @param string $spec_id
   *   The export specification ID.
   * @param array $input
   *   The input array.
   * @param array $context
   *   The batch context.
   */
  public function operation(
    string $spec_id,
    array $input,
    array &$context
  ): void;

  /**
   * Handles the batch finished callback.
   *
   * @param bool $success
   *   Indicates the success of operation.
   * @param array $results
   *   Batch process results array.
   */
  public function finished(
    $success,
    array $results
  ): void;

}
