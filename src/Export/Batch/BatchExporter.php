<?php

namespace Drupal\data_transfer\Export\Batch;

use Drupal\data_transfer\Export\ExporterBase;
use Drupal\data_transfer\Export\ExportResultInterface;
use Drupal\data_transfer\Export\Loader\ExportDataLoaderInterface;
use Drupal\data_transfer\Export\Writer\ExportWriterInterface;
use Drupal\data_transfer\Plugin\PluginWithStorageInterface;

/**
 * Provides exporter for batch processing.
 *
 * @see \Drupal\data_transfer\Export\Batch\ExportBatchHandler
 */
class BatchExporter extends ExporterBase implements BatchExporterInterface {

  /**
   * The batch context key of the total number of records to export.
   */
  public const TOTAL_KEY = 'total';

  /**
   * The batch context key holding the number of processed records.
   */
  public const PROCESSED_KEY = 'processed';

  /**
   * The batch context key holding the loader storage.
   */
  public const LOADER_STORAGE_KEY = 'loader_storage';

  /**
   * The batch context key holding the writer storage.
   */
  public const WRITER_STORAGE_KEY = 'writer_storage';

  /**
   * The batch context key holding the export result storage.
   */
  public const RESULT_STORAGE_KEY = 'result_storage';

  /**
   * {@inheritdoc}
   */
  public function exportRecordSet(array &$context): void {
    $sandbox =& $context['sandbox'];
    $loader = $this->getLoader($sandbox[static::LOADER_STORAGE_KEY] ?? []);

    $is_init = empty($sandbox);
    if ($is_init) {
      $sandbox[static::TOTAL_KEY] = $loader->getTotalCount();
      $sandbox[static::PROCESSED_KEY] = 0;
      $sandbox[static::LOADER_STORAGE_KEY] = [];
      $sandbox[static::WRITER_STORAGE_KEY] = [];
      $sandbox[static::RESULT_STORAGE_KEY] = [];
    }

    $result = $this->createExportResult($sandbox[static::RESULT_STORAGE_KEY]);
    $writer = $this->getWriter($sandbox[static::WRITER_STORAGE_KEY]);

    if ($is_init) {
      $result->setTotal($sandbox[static::TOTAL_KEY]);
      $writer->init();
    }

    $total = $sandbox[static::TOTAL_KEY];
    $processed = $sandbox[static::PROCESSED_KEY];
    $limit = $this->exportSpecification->getBatchRecordsLimit();

    $ids = $loader->loadIds($processed, $limit);
    $data = $loader->loadData($ids);
    $data = $this->transformData($data);
    $data = $this->normalizeData($data);
    $writer->open();
    $writer->write($data, $result);
    $writer->close();

    $processed += $limit;

    if (empty($ids)) {
      $finished = 1;
    }
    else {
      $finished = $processed / $total;
    }
    $context['finished'] = $finished;

    if ($finished >= 1) {
      $writer->finish($result);
      $context['results'][] = $result->toArray();
    }
    else {
      $sandbox[static::PROCESSED_KEY] = $processed;
      $this->persistStorages($sandbox, $loader, $writer, $result);
    }
  }

  /**
   * Saves storage of different objects into the batch sandbox.
   *
   * @param array $sandbox
   *   The batch sandbox.
   * @param \Drupal\data_transfer\Export\Loader\ExportDataLoaderInterface $loader
   *   The loader plugin.
   * @param \Drupal\data_transfer\Export\Writer\ExportWriterInterface $writer
   *   The writer plugin.
   * @param \Drupal\data_transfer\Export\ExportResultInterface $result
   *   The export result.
   */
  protected function persistStorages(
    array &$sandbox,
    ExportDataLoaderInterface $loader,
    ExportWriterInterface $writer,
    ExportResultInterface $result
  ): void {
    if ($loader instanceof PluginWithStorageInterface) {
      $sandbox[static::LOADER_STORAGE_KEY] = $loader->getStorage()
        ->toArray();
    }

    if ($writer instanceof PluginWithStorageInterface) {
      $sandbox[static::WRITER_STORAGE_KEY] = $writer->getStorage()
        ->toArray();
    }

    $sandbox[static::RESULT_STORAGE_KEY] = $result->toArray();
  }

}
