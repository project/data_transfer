<?php

namespace Drupal\data_transfer\Export;

/**
 * Interface of a class that does the export of the data.
 */
interface ExporterInterface {

  /**
   * Exports the data.
   *
   * @return \Drupal\data_transfer\Export\ExportResultInterface
   *   The export result.
   */
  public function export(): ExportResultInterface;

}
