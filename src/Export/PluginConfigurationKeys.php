<?php

namespace Drupal\data_transfer\Export;

/**
 * Dictionary class for common keys in the export plugin configuration.
 */
final class PluginConfigurationKeys {

  /**
   * The input key.
   */
  public const INPUT = 'input';

}
