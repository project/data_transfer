<?php

namespace Drupal\data_transfer\Export;

use Drupal\data_transfer\Entity\ExportSpecificationInterface;
use Drupal\data_transfer\Export\Loader\ExportDataLoaderInterface;
use Drupal\data_transfer\Export\Loader\ExportLoaderPluginManagerInterface;
use Drupal\data_transfer\Export\Normalizer\ExportNormalizerPluginManagerInterface;
use Drupal\data_transfer\Export\Transformer\ExportTransformerPluginManagerInterface;
use Drupal\data_transfer\Export\Writer\ExportWriterInterface;
use Drupal\data_transfer\Export\Writer\ExportWriterPluginManagerInterface;
use Drupal\data_transfer\Plugin\PluginStorage;
use Drupal\data_transfer\Plugin\PluginWithStorageInterface;

/**
 * Provides base class for different types of exporters.
 */
abstract class ExporterBase {

  /**
   * The export specification to follow.
   *
   * @var \Drupal\data_transfer\Entity\ExportSpecificationInterface
   */
  protected $exportSpecification;

  /**
   * The loader plugin manager.
   *
   * @var \Drupal\data_transfer\Export\Loader\ExportLoaderPluginManagerInterface
   */
  protected $loaderManager;

  /**
   * The transformer plugin manager.
   *
   * @var \Drupal\data_transfer\Export\Transformer\ExportTransformerPluginManagerInterface
   */
  protected $transformerManager;

  /**
   * The normalizer plugin manager.
   *
   * @var \Drupal\data_transfer\Export\Normalizer\ExportNormalizerPluginManagerInterface
   */
  protected $normalizerManager;

  /**
   * The writer plugin manager.
   *
   * @var \Drupal\data_transfer\Export\Writer\ExportWriterPluginManagerInterface
   */
  protected $writerManager;

  /**
   * The input array passed to the loader/normalizer/writer plugin configs.
   *
   * @var array
   */
  protected $input = [];

  /**
   * A constructor.
   *
   * @param \Drupal\data_transfer\Entity\ExportSpecificationInterface $export_specification
   *   The export specification to follow.
   * @param array $input
   *   The input array passed to the loader/normalizer/writer plugin configs.
   * @param \Drupal\data_transfer\Export\Loader\ExportLoaderPluginManagerInterface $loader_manager
   *   The loader plugin manager.
   * @param \Drupal\data_transfer\Export\Transformer\ExportTransformerPluginManagerInterface $transformer_manager
   *   The transformer plugin manager.
   * @param \Drupal\data_transfer\Export\Normalizer\ExportNormalizerPluginManagerInterface $normalizer_manager
   *   The normalizer plugin manager.
   * @param \Drupal\data_transfer\Export\Writer\ExportWriterPluginManagerInterface $writer_manager
   *   The writer plugin manager.
   */
  public function __construct(
    ExportSpecificationInterface $export_specification,
    array $input,
    ExportLoaderPluginManagerInterface $loader_manager,
    ExportTransformerPluginManagerInterface $transformer_manager,
    ExportNormalizerPluginManagerInterface $normalizer_manager,
    ExportWriterPluginManagerInterface $writer_manager
  ) {
    $this->exportSpecification = $export_specification;
    $this->input = $input;
    $this->loaderManager = $loader_manager;
    $this->transformerManager = $transformer_manager;
    $this->normalizerManager = $normalizer_manager;
    $this->writerManager = $writer_manager;
  }

  /**
   * Returns instance of the loader plugin.
   *
   * @param array $storage_data
   *   The data to populate the plugin storage with.
   *
   * @return \Drupal\data_transfer\Export\Loader\ExportDataLoaderInterface
   *   The loader.
   */
  protected function getLoader(
    array $storage_data = []
  ): ExportDataLoaderInterface {
    $plugin_spec = $this->exportSpecification->getLoaderPluginSpecification();
    $loader = $this->loaderManager
      ->createFromSpecification($plugin_spec, $this->input);

    if ($loader instanceof PluginWithStorageInterface) {
      $storage = new PluginStorage($storage_data);
      $loader->setStorage($storage);
    }

    return $loader;
  }

  /**
   * Returns the list of transformer plugins.
   *
   * @return \Drupal\data_transfer\Export\Transformer\ExportTransformerInterface[]
   *   The transformers.
   */
  protected function getTransformers(): array {
    $plugins = [];

    $plugin_specs = $this->exportSpecification
      ->getTransformerPluginSpecifications();
    foreach ($plugin_specs as $key => $plugin_spec) {
      $plugins[$key] = $this->transformerManager
        ->createFromSpecification($plugin_spec, $this->input);
    }

    return $plugins;
  }

  /**
   * Returns the list of normalizer plugins.
   *
   * @return \Drupal\data_transfer\Export\Normalizer\ExportNormalizerInterface[]
   *   The normalizers.
   */
  protected function getNormalizers(): array {
    $plugins = [];

    $plugin_specs = $this->exportSpecification
      ->getNormalizerPluginSpecifications();
    foreach ($plugin_specs as $key => $plugin_spec) {
      $plugins[$key] = $this->normalizerManager
        ->createFromSpecification($plugin_spec, $this->input);
    }

    return $plugins;
  }

  /**
   * Returns instance of the writer plugin.
   *
   * @param array $storage_data
   *   The data to populate the plugin storage with.
   *
   * @return \Drupal\data_transfer\Export\Writer\ExportWriterInterface
   *   The writer.
   */
  protected function getWriter(
    array $storage_data = []
  ): ExportWriterInterface {
    $plugin_spec = $this->exportSpecification->getWriterPluginSpecification();
    $writer = $this->writerManager
      ->createFromSpecification($plugin_spec, $this->input);

    if ($writer instanceof PluginWithStorageInterface) {
      $storage = new PluginStorage($storage_data);
      $writer->setStorage($storage);
    }

    return $writer;
  }

  /**
   * Applies all the transformations to the data records.
   *
   * @param array $data
   *   The data records.
   *
   * @return array
   *   The transformed data records.
   */
  protected function transformData(array $data): array {
    $transformers = $this->getTransformers();

    foreach ($transformers as $transformer) {
      $transformer->transform($data);
    }

    return $data;
  }

  /**
   * Normalizes the input data using normalizer plugins.
   *
   * @param array $input_data
   *   The input data got from the loader.
   *
   * @return array
   *   The normalized data.
   */
  protected function normalizeData(array $input_data): array {
    $normalizers = $this->getNormalizers();

    $result = [];
    foreach ($input_data as $key => $input_record) {
      $result_record = [];
      foreach ($normalizers as $normalizer) {
        $normalizer->normalize($input_record, $result_record);
      }
      $result[$key] = $result_record;
    }
    return $result;
  }

  /**
   * Creates an instance of the export result.
   *
   * @return \Drupal\data_transfer\Export\ExportResultInterface
   *   The export result.
   */
  protected function createExportResult(
    array $storage = []
  ): ExportResultInterface {
    return new ExportResult($storage);
  }

}
