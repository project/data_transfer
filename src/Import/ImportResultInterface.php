<?php

namespace Drupal\data_transfer\Import;

/**
 * Represents result of a single import.
 */
interface ImportResultInterface {

  /**
   * Builds array representation of the result.
   *
   * Result of this method could be passed to the class constructor in order to
   * re-create exactly the same result. It should be used to persist the result
   * between requests.
   *
   * @return array
   *   The array representation of the result.
   */
  public function toArray(): array;

  /**
   * Sets the total number of records.
   *
   * @param int $total
   *   The number of total records to import.
   *
   * @return static
   *   Self.
   */
  public function setTotal(int $total): ImportResultInterface;

  /**
   * Returns the number of items to import.
   *
   * @return int
   *   The number of items to import.
   */
  public function getTotal(): int;

  /**
   * Increases the number of source records that were actually read.
   *
   * @param int $number
   *   The number to increase with.
   *
   * @return static
   *   Self.
   */
  public function addSourceNumber(int $number): ImportResultInterface;

  /**
   * Returns the number of source records that were read.
   *
   * @return int
   *   The number of source records.
   */
  public function getSourceNumber(): int;

  /**
   * Increases the number of records that were written successfully.
   *
   * @param int $number
   *   The number to increase with.
   *
   * @return static
   *   Self.
   */
  public function addTargetNumber(int $number): ImportResultInterface;

  /**
   * Returns the number of records that were written by the writer.
   *
   * @return int
   *   The number of target records.
   */
  public function getTargetNumber(): int;

  /**
   * Returns the list of links added to the result.
   *
   * @return array
   *   The list of links. Every item is an array with the following keys:
   *   - link: the link text,
   *   - url: the link URL string.
   */
  public function getLinks(): array;

  /**
   * Returns TRUE if there are any links on the import result.
   *
   * @return bool
   *   TRUE if the result has links.
   */
  public function hasLinks(): bool;

  /**
   * Adds link to the import result.
   *
   * @param string $text
   *   The link text.
   * @param string $url
   *   The link URL.
   */
  public function addLink(string $text, string $url): void;

}
