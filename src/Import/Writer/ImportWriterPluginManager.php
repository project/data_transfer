<?php

namespace Drupal\data_transfer\Import\Writer;

use Drupal\data_transfer\Plugin\PluginManagerBase;

/**
 * Plugin manager of the import readers.
 */
class ImportWriterPluginManager extends PluginManagerBase implements ImportWriterPluginManagerInterface {

  /**
   * {@inheritdoc}
   */
  public const SUBDIR = 'Plugin/data_transfer/ImportWriter';

  /**
   * {@inheritdoc}
   */
  public const CACHE_KEY = 'import_writer';

  /**
   * {@inheritdoc}
   */
  public const PLUGIN_INTERFACE = ImportWriterInterface::class;

}
