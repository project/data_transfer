<?php

namespace Drupal\data_transfer\Import\Writer;

use Drupal\data_transfer\Import\Record\ImportRecordSetInterface;

/**
 * The writer purpose is to save the target records to the permanent storage.
 */
interface ImportWriterInterface {

  /**
   * Writes records into the permanent storage.
   *
   * Usually it should load/create the target objects, set all the
   * properties/fields to these object from the records and save them all
   * to the permanent storage.
   *
   * @param \Drupal\data_transfer\Import\Record\ImportRecordSetInterface $record_set
   *   The record set to write to the storage. The processed data is in the
   *   source of every record, target data could be used for any purpose.
   */
  public function writeTargetRecords(
    ImportRecordSetInterface $record_set
  ): void;

}
