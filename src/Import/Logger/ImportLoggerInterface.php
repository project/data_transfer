<?php

namespace Drupal\data_transfer\Import\Logger;

use Drupal\data_transfer\Import\ImportResultInterface;

/**
 * The import logger.
 *
 * It is responsible for logging error that appear during import.
 */
interface ImportLoggerInterface {

  /**
   * Logs the import start.
   *
   * Gets called only once per import.
   *
   * @param \Drupal\data_transfer\Import\ImportResultInterface $result
   *   The import result with the total number populated only.
   */
  public function logImportStart(ImportResultInterface $result): void;

  /**
   * Logs the import finish.
   *
   * Gets called only once per import.
   *
   * @param \Drupal\data_transfer\Import\ImportResultInterface $result
   *   The import result.
   */
  public function logImportFinish(ImportResultInterface $result): void;

  /**
   * Logs start of the import of a record set.
   *
   * @param \Drupal\data_transfer\Import\ImportResultInterface $result
   *   The import result.
   */
  public function logRecordSetStart(ImportResultInterface $result): void;

  /**
   * Logs end of the import of a record set.
   *
   * @param \Drupal\data_transfer\Import\ImportResultInterface $result
   *   The import result.
   */
  public function logRecordSetFinish(ImportResultInterface $result): void;

  /**
   * Logs a generic import error.
   *
   * This kind of import is not about a row, but about the overall process.
   *
   * @param string $error
   *   The error.
   */
  public function logGenericError(string $error): void;

  /**
   * Logs the import row error.
   *
   * This kind of error clearly indicate there is something wrong about some
   * specific row.
   *
   * @param string $key
   *   The row key.
   * @param string $error
   *   The error.
   */
  public function logRowError(string $key, string $error): void;

}
