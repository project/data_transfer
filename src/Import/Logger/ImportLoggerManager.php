<?php

namespace Drupal\data_transfer\Import\Logger;

use Drupal\data_transfer\Plugin\PluginManagerBase;

/**
 * Plugin manager of the import loggers.
 */
class ImportLoggerManager extends PluginManagerBase implements ImportLoggerManagerInterface {

  /**
   * {@inheritdoc}
   */
  public const SUBDIR = 'Plugin/data_transfer/ImportLogger';

  /**
   * {@inheritdoc}
   */
  public const CACHE_KEY = 'import_logger';

  /**
   * {@inheritdoc}
   */
  public const PLUGIN_INTERFACE = ImportLoggerInterface::class;

}
