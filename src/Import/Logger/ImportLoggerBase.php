<?php

namespace Drupal\data_transfer\Import\Logger;

use Drupal\data_transfer\Import\ImportResultInterface;
use Drupal\data_transfer\Plugin\PluginBase;

/**
 * Provides base class for the import logger plug-ins.
 */
abstract class ImportLoggerBase extends PluginBase implements ImportLoggerInterface {

  /**
   * {@inheritdoc}
   */
  public function logImportStart(ImportResultInterface $result): void {
    // Nothing by default.
  }

  /**
   * {@inheritdoc}
   */
  public function logImportFinish(ImportResultInterface $result): void {
    // Nothing by default.
  }

  /**
   * {@inheritdoc}
   */
  public function logRecordSetStart(ImportResultInterface $result): void {
    // Nothing by default.
  }

  /**
   * {@inheritdoc}
   */
  public function logRecordSetFinish(ImportResultInterface $result): void {
    // Nothing by default.
  }

}
