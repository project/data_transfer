<?php

namespace Drupal\data_transfer\Import;

use Drupal\data_transfer\Entity\ImportSpecificationInterface;
use Drupal\data_transfer\Exception\ThrowableLoggerTrait;
use Drupal\data_transfer\Import\Logger\ImportLoggerInterface;
use Drupal\data_transfer\Import\Logger\ImportLoggerManagerInterface;
use Drupal\data_transfer\Import\Mapper\ImportMapperInterface;
use Drupal\data_transfer\Import\Mapper\ImportMapperPluginManagerInterface;
use Drupal\data_transfer\Import\Processor\ImportProcessorPluginManagerInterface;
use Drupal\data_transfer\Import\Reader\ImportReaderInterface;
use Drupal\data_transfer\Import\Reader\ImportReaderPluginManagerInterface;
use Drupal\data_transfer\Import\Record\EmptyImportRecordSet;
use Drupal\data_transfer\Import\Record\ImportRecordSetInterface;
use Drupal\data_transfer\Import\Writer\ImportWriterInterface;
use Drupal\data_transfer\Import\Writer\ImportWriterPluginManagerInterface;
use Drupal\data_transfer\Plugin\PluginStorage;
use Drupal\data_transfer\Plugin\PluginWithStorageInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides base class for different types of importers.
 */
abstract class ImporterBase {

  use ThrowableLoggerTrait;

  /**
   * The class name used to create an empty record set.
   */
  public const EMPTY_RECORD_SET_CLASS = EmptyImportRecordSet::class;

  /**
   * The class name of the import result.
   */
  public const IMPORT_RESULT_CLASS = ImportResult::class;

  /**
   * The import specification to follow.
   *
   * @var \Drupal\data_transfer\Entity\ImportSpecificationInterface
   */
  protected $importSpecification;

  /**
   * The reader plugin manager.
   *
   * @var \Drupal\data_transfer\Import\Reader\ImportReaderPluginManagerInterface
   */
  protected $readerManager;

  /**
   * The mapper plugin manager.
   *
   * @var \Drupal\data_transfer\Import\Mapper\ImportMapperPluginManagerInterface
   */
  protected $mapperManager;

  /**
   * The processor plugin manager.
   *
   * @var \Drupal\data_transfer\Import\Processor\ImportProcessorPluginManagerInterface
   */
  protected $processorManager;

  /**
   * The writer plugin manager.
   *
   * @var \Drupal\data_transfer\Import\Writer\ImportWriterPluginManager
   */
  protected $writerManager;

  /**
   * The logger plugin manager.
   *
   * @var \Drupal\data_transfer\Import\Logger\ImportLoggerManagerInterface
   */
  protected $loggerManager;

  /**
   * The system logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $systemLogger;

  /**
   * The input array passed to the loader/normalizer/writer plugin configs.
   *
   * @var array
   */
  protected $input = [];

  /**
   * A constructor.
   */
  public function __construct(
    ImportSpecificationInterface $import_specification,
    array $input,
    ImportReaderPluginManagerInterface $reader_manager,
    ImportMapperPluginManagerInterface $mapper_manager,
    ImportProcessorPluginManagerInterface $processor_manager,
    ImportWriterPluginManagerInterface $writer_manager,
    ImportLoggerManagerInterface $logger_manager,
    LoggerInterface $system_logger
  ) {
    $this->importSpecification = $import_specification;
    $this->input = $input;
    $this->readerManager = $reader_manager;
    $this->mapperManager = $mapper_manager;
    $this->processorManager = $processor_manager;
    $this->writerManager = $writer_manager;
    $this->loggerManager = $logger_manager;
    $this->systemLogger = $system_logger;
  }

  /**
   * Returns instance of the loader plugin.
   *
   * @param array $storage_data
   *   The data to populate the plugin storage with.
   *
   * @return \Drupal\data_transfer\Import\Reader\ImportReaderInterface
   *   The reader.
   */
  protected function getReader(
    array $storage_data = []
  ): ImportReaderInterface {
    $plugin_spec = $this->importSpecification->getReaderPluginSpecification();
    $reader = $this->readerManager
      ->createFromSpecification($plugin_spec, $this->input);

    if ($reader instanceof PluginWithStorageInterface) {
      $storage = new PluginStorage($storage_data);
      $reader->setStorage($storage);
    }

    return $reader;
  }

  /**
   * Returns the list of transformer plugins.
   *
   * @return \Drupal\data_transfer\Import\Mapper\ImportMapperInterface
   *   The mapper.
   */
  protected function getMapper(): ImportMapperInterface {
    $plugin_specs = $this->importSpecification
      ->getMapperPluginSpecification();
    $mapper = $this->mapperManager
      ->createFromSpecification($plugin_specs, $this->input);

    return $mapper;
  }

  /**
   * Returns the list of normalizer plugins.
   *
   * @return \Drupal\data_transfer\Import\Processor\ImportProcessorInterface[]
   *   The processors.
   */
  protected function getProcessors(): array {
    $plugins = [];

    $plugin_specs = $this->importSpecification
      ->getProcessorPluginSpecifications();
    foreach ($plugin_specs as $key => $plugin_spec) {
      $plugins[$key] = $this->processorManager
        ->createFromSpecification($plugin_spec, $this->input);
    }

    return $plugins;
  }

  /**
   * Returns instance of the writer plugin.
   *
   * @param array $storage_data
   *   The data to populate the plugin storage with.
   *
   * @return \Drupal\data_transfer\Import\Writer\ImportWriterInterface
   *   The writer.
   */
  protected function getWriter(
    array $storage_data = []
  ): ImportWriterInterface {
    $plugin_spec = $this->importSpecification->getWriterPluginSpecification();
    $writer = $this->writerManager
      ->createFromSpecification($plugin_spec, $this->input);

    if ($writer instanceof PluginWithStorageInterface) {
      $storage = new PluginStorage($storage_data);
      $writer->setStorage($storage);
    }

    return $writer;
  }

  /**
   * Returns instance of the logger plugin.
   *
   * @param array $storage_data
   *   The data to populate the plugin storage with.
   *
   * @return \Drupal\data_transfer\Import\Logger\ImportLoggerInterface
   *   The logger.
   */
  protected function getLogger(
    array $storage_data = []
  ): ImportLoggerInterface {
    $plugin_spec = $this->importSpecification->getLoggerPluginSpecification();
    $logger = $this->loggerManager
      ->createFromSpecification($plugin_spec, $this->input);

    if ($logger instanceof PluginWithStorageInterface) {
      $storage = new PluginStorage($storage_data);
      $logger->setStorage($storage);
    }

    return $logger;
  }

  /**
   * Creates an instance of the import result.
   *
   * @param array $storage
   *   The storage.
   *
   * @return \Drupal\data_transfer\Import\ImportResultInterface
   *   The import result.
   */
  protected function createImportResult(
    array $storage = []
  ): ImportResultInterface {
    $class = static::IMPORT_RESULT_CLASS;
    return new $class($storage);
  }

  /**
   * Reads records using the passed reader.
   *
   * @param \Drupal\data_transfer\Import\Reader\ImportReaderInterface $reader
   *   The reader to use.
   * @param int|null $limit
   *   The limit or NULL to read all the available records.
   * @param \Drupal\data_transfer\Import\ImportResultInterface $result
   *   The import result.
   * @param \Drupal\data_transfer\Import\Logger\ImportLoggerInterface $logger
   *   The import logger.
   *
   * @return \Drupal\data_transfer\Import\Record\ImportRecordSetInterface
   *   The record set.
   */
  protected function readRecords(
    ImportReaderInterface $reader,
    ?int $limit,
    ImportResultInterface $result,
    ImportLoggerInterface $logger
  ): ImportRecordSetInterface {
    try {
      $record_set = $reader->readNextRecordSet($limit);
      $result->addSourceNumber(count($record_set));
      $this->logRecordSetErrors($record_set, $logger);
      return $record_set->cloneWithValidRecords(FALSE);
    }
    catch (\Throwable $e) {
      $this->logThrowable($e);
      $logger->logGenericError($this->t(
        'Unexpected error appeared while reading the source data. See system log for details.'
      ));
      return $this->createEmptyRecordSet();
    }
  }

  /**
   * Maps the source record properties into the ones expected by the processors.
   *
   * @param \Drupal\data_transfer\Import\Mapper\ImportMapperInterface $mapper
   *   The mapper to use.
   * @param \Drupal\data_transfer\Import\Record\ImportRecordSetInterface $record_set
   *   The record set to map.
   * @param \Drupal\data_transfer\Import\Logger\ImportLoggerInterface $logger
   *   The import logger.
   *
   * @return \Drupal\data_transfer\Import\Record\ImportRecordSetInterface
   *   The record set with the mapped data in the record source data.
   */
  protected function mapRecords(
    ImportMapperInterface $mapper,
    ImportRecordSetInterface $record_set,
    ImportLoggerInterface $logger
  ): ImportRecordSetInterface {
    try {
      $mapper->mapRecords($record_set);
      $this->logRecordSetErrors($record_set, $logger);
      return $record_set->cloneWithValidRecords(TRUE);
    }
    catch (\Throwable $e) {
      $this->logThrowable($e);
      $logger->logGenericError($this->t(
        'Unexpected error appeared while mapping the source data. See system log for details.'
      ));
      return $this->createEmptyRecordSet();
    }
  }

  /**
   * Processes all the passed source records to build the target records.
   *
   * @param \Drupal\data_transfer\Import\Processor\ImportProcessorInterface[] $processors
   *   The list of processors to run on every record.
   * @param \Drupal\data_transfer\Import\Record\ImportRecordSetInterface $record_set
   *   The record set to process.
   * @param \Drupal\data_transfer\Import\Logger\ImportLoggerInterface $logger
   *   The import logger.
   *
   * @return \Drupal\data_transfer\Import\Record\ImportRecordSetInterface
   *   The processed record set. Processed target data is set as source data on
   *   the records.
   */
  protected function processRecords(
    array $processors,
    ImportRecordSetInterface $record_set,
    ImportLoggerInterface $logger
  ): ImportRecordSetInterface {
    try {
      $last_index = count($processors) - 1;

      foreach (array_values($processors) as $index => $processor) {
        $processor->processRecords($record_set);
        $this->logRecordSetErrors($record_set, $logger);

        // Clone target to source on last processor.
        $is_last = ($index === $last_index);
        $record_set = $record_set->cloneWithValidRecords($is_last);
      }
      return $record_set;
    }
    catch (\Throwable $e) {
      $this->logThrowable($e);
      $logger->logGenericError($this->t(
        'Unexpected error appeared while processing the import records. See system log for details.'
      ));
      return $this->createEmptyRecordSet();
    }
  }

  /**
   * Writes multiple target records into the permanent storage.
   *
   * @param \Drupal\data_transfer\Import\Writer\ImportWriterInterface $writer
   *   The writer to use.
   * @param \Drupal\data_transfer\Import\Record\ImportRecordSetInterface $record_set
   *   The record set to write. Source data of every record contains processed
   *   data, target data is empty and could be used for any purpose.
   * @param \Drupal\data_transfer\Import\ImportResultInterface $result
   *   The import result.
   * @param \Drupal\data_transfer\Import\Logger\ImportLoggerInterface $logger
   *   The import logger.
   */
  protected function writeRecords(
    ImportWriterInterface $writer,
    ImportRecordSetInterface $record_set,
    ImportResultInterface $result,
    ImportLoggerInterface $logger
  ): void {
    try {
      $writer->writeTargetRecords($record_set);
      $this->logRecordSetErrors($record_set, $logger);
      $record_set = $record_set->cloneWithValidRecords();
      $result->addTargetNumber(count($record_set));
    }
    catch (\Throwable $e) {
      $this->logThrowable($e);
      $logger->logGenericError($this->t(
        'Unexpected error appeared while saving the import records. See system log for details.'
      ));
    }
  }

  /**
   * Logs errors of all the records.
   *
   * @param \Drupal\data_transfer\Import\Record\ImportRecordSetInterface $record_set
   *   The record set.
   * @param \Drupal\data_transfer\Import\Logger\ImportLoggerInterface $logger
   *   The logger.
   */
  protected function logRecordSetErrors(
    ImportRecordSetInterface $record_set,
    ImportLoggerInterface $logger
  ): void {
    foreach ($record_set as $key => $record) {
      foreach ($record->getErrors() as $errors) {
        if (!is_array($errors)) {
          $errors = [$errors];
        }

        foreach ($errors as $error) {
          // @todo We loose the error path here. Find a way to keep it.
          $logger->logRowError($key, $error);
        }
      }
    }
  }

  /**
   * Creates an empty record set with no records.
   *
   * @return \Drupal\data_transfer\Import\Record\ImportRecordSetInterface
   *   The empty record set.
   */
  protected function createEmptyRecordSet(): ImportRecordSetInterface {
    $class = static::EMPTY_RECORD_SET_CLASS;
    return new $class();
  }

  /**
   * {@inheritdoc}
   */
  protected function getThrowableLogger(): LoggerInterface {
    return $this->systemLogger;
  }

}
