<?php

namespace Drupal\data_transfer\Import;

/**
 * Represents result of a single import.
 */
class ImportResult implements ImportResultInterface {

  /**
   * The key of the storage that holds the list of URLs.
   */
  public const LINKS_KEY = 'links';

  /**
   * The key of the storage that holds the number of total records to import.
   */
  public const TOTAL_COUNT_KEY = 'total';

  /**
   * The key of the storage that holds the number of source records.
   */
  public const SOURCE_COUNT_KEY = 'source';

  /**
   * The key of the storage that holds the number of target records.
   */
  public const TARGET_COUNT_KEY = 'target';

  /**
   * The key of the link label.
   */
  public const LINK_LABEL_KEY = 'label';

  /**
   * The key of the link url.
   */
  public const LINK_URL_KEY = 'url';

  /**
   * The actual data storage.
   *
   * @var array
   */
  protected $storage = [];

  /**
   * A constructor.
   *
   * @param array $storage
   *   The data storage, as returned by ::toArray() method.
   */
  public function __construct(array $storage = []) {
    $storage += [
      static::LINKS_KEY => [],
      static::TOTAL_COUNT_KEY => 0,
      static::SOURCE_COUNT_KEY => 0,
      static::TARGET_COUNT_KEY => 0,
    ];
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  public function toArray(): array {
    return $this->storage;
  }

  /**
   * {@inheritdoc}
   */
  public function setTotal(int $total): ImportResultInterface {
    $this->storage[static::TOTAL_COUNT_KEY] = $total;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotal(): int {
    return $this->storage[static::TOTAL_COUNT_KEY];
  }

  /**
   * {@inheritdoc}
   */
  public function addSourceNumber(int $add): ImportResultInterface {
    $this->storage += [
      static::SOURCE_COUNT_KEY => 0,
    ];
    $this->storage[static::SOURCE_COUNT_KEY] += $add;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceNumber(): int {
    return $this->storage[static::SOURCE_COUNT_KEY];
  }

  /**
   * {@inheritdoc}
   */
  public function addTargetNumber(int $add): ImportResultInterface {
    $this->storage += [
      static::TARGET_COUNT_KEY => 0,
    ];
    $this->storage[static::TARGET_COUNT_KEY] += $add;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetNumber(): int {
    return $this->storage[static::TARGET_COUNT_KEY];
  }

  /**
   * {@inheritdoc}
   */
  public function hasLinks(): bool {
    return !empty($this->storage[static::LINKS_KEY]);
  }

  /**
   * {@inheritdoc}
   */
  public function getLinks(): array {
    return $this->storage[static::LINKS_KEY] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function addLink(string $text, string $url): void {
    $this->storage[static::LINKS_KEY][] = [
      static::LINK_LABEL_KEY => $text,
      static::LINK_URL_KEY => $url,
    ];
  }

}
