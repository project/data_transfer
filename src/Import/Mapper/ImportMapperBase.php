<?php

namespace Drupal\data_transfer\Import\Mapper;

use Drupal\data_transfer\Exception\ExchangeSkipRowException;
use Drupal\data_transfer\Import\Record\ImportRecordInterface;
use Drupal\data_transfer\Import\Record\ImportRecordSetInterface;
use Drupal\data_transfer\Plugin\PluginBase;

/**
 * Provides base class for simple import mappers that process records 1-by-1.
 */
abstract class ImportMapperBase extends PluginBase implements ImportMapperInterface {

  /**
   * {@inheritdoc}
   */
  public function mapRecords(ImportRecordSetInterface $record_set): void {
    foreach ($record_set as $record) {
      try {
        $this->mapRecord($record);
      }
      catch (ExchangeSkipRowException $e) {
        $record->addError($e->getMessage());
      }
    }
  }

  /**
   * Maps properties of a single record.
   *
   * @param \Drupal\data_transfer\Import\Record\ImportRecordInterface $record
   *   The record to map the properties of.
   *
   * @throws \Drupal\data_transfer\Exception\ExchangeSkipRowException
   *   Thrown in case the row isn't valid and should be excluded from further
   *   processing. It actually just adds an error message of the exception to
   *   the row.
   */
  abstract protected function mapRecord(ImportRecordInterface $record): void;

}
