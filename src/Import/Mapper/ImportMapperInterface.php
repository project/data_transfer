<?php

namespace Drupal\data_transfer\Import\Mapper;

use Drupal\data_transfer\Import\Record\ImportRecordSetInterface;

/**
 * Represents the source record mapper of the import process.
 *
 * Its goal is to take raw record produced by the reader plugin and put its
 * properties under proper keys configured through the user input.
 */
interface ImportMapperInterface {

  /**
   * Maps the source record properties into the ones expected by the processors.
   *
   * The rows with errors are excluded from further processing and writing.
   *
   * @param \Drupal\data_transfer\Import\Record\ImportRecordSetInterface $record_set
   *   The record set with the source data provided by the reader.
   */
  public function mapRecords(ImportRecordSetInterface $record_set): void;

}
