<?php

namespace Drupal\data_transfer\Import\Mapper;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\data_transfer\Plugin\PluginSpecificationInterface;

/**
 * Plugin manager of the import mappers.
 */
interface ImportMapperPluginManagerInterface extends PluginManagerInterface {

  /**
   * Creates plugin instance from its specification.
   *
   * @param \Drupal\data_transfer\Plugin\PluginSpecificationInterface $specification
   *   The plugin specification.
   * @param array $input
   *   The input to add to the plugin configuration.
   *
   * @return \Drupal\data_transfer\Import\Mapper\ImportMapperInterface
   *   The mapper.
   */
  public function createFromSpecification(
    PluginSpecificationInterface $specification,
    array $input = []
  );

}
