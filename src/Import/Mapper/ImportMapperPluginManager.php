<?php

namespace Drupal\data_transfer\Import\Mapper;

use Drupal\data_transfer\Plugin\PluginManagerBase;

/**
 * Plugin manager of the import mappers.
 */
class ImportMapperPluginManager extends PluginManagerBase implements ImportMapperPluginManagerInterface {

  /**
   * {@inheritdoc}
   */
  public const SUBDIR = 'Plugin/data_transfer/ImportMapper';

  /**
   * {@inheritdoc}
   */
  public const CACHE_KEY = 'import_mapper';

  /**
   * {@inheritdoc}
   */
  public const PLUGIN_INTERFACE = ImportMapperInterface::class;

}
