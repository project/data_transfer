<?php

namespace Drupal\data_transfer\Import\Processor;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\data_transfer\Exception\ExchangeSkipRowException;
use Drupal\data_transfer\Plugin\ConfigurationReadingPluginInterface;
use Drupal\data_transfer\Import\Record\ImportRecordInterface;
use Drupal\data_transfer\Import\Record\ImportRecordSetInterface;
use Drupal\data_transfer\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base class for the import data processors.
 *
 * Config options:
 * - from: the source path specification,
 * - to: the target path.
 *
 * @see \Drupal\data_transfer\Exchange\RecordPath\RecordPathParserInterface::parsePath()
 */
abstract class ImportProcessorBase extends PluginBase implements ImportProcessorInterface, ContainerFactoryPluginInterface, ConfigurationReadingPluginInterface {

  /**
   * The source path setting key.
   */
  public const SOURCE_PATH_SETTING = 'from';

  /**
   * The target path setting key.
   */
  public const TARGET_PATH_SETTING = 'to';

  /**
   * Whether the source path setting is required or not.
   *
   * Child class could set it to FALSE in order to avoid exceptions on missing
   * source path in the plugin config.
   */
  public const SOURCE_PATH_REQUIRED = TRUE;

  /**
   * Whether the target path setting is required or not.
   *
   * Child class could set it to FALSE in order to avoid exceptions on missing
   * target path in the plugin config.
   */
  public const TARGET_PATH_REQUIRED = TRUE;

  /**
   * The record path parser.
   *
   * @var \Drupal\data_transfer\Exchange\RecordPath\RecordPathParserInterface
   */
  protected $recordPathParser;

  /**
   * The source path specification.
   *
   * @var \Drupal\data_transfer\Exchange\RecordPath\RecordPathSpecInterface|null
   */
  protected $sourcePathSpec;

  /**
   * The target path of the import record to write to.
   *
   * It's NULL in case the setting is missing and the plugin allows it.
   *
   * @var array|null
   */
  protected $targetPath;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->recordPathParser = $container->get(
      'data_transfer.record_path_parser'
    );

    return $instance;
  }

  /**
   * Reads configuration array to the plugin properties.
   *
   * Gets called from the class constructor.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  public function readConfiguration(): void {
    $this->readSourcePathConfig();
    $this->readTargetPathConfig();
  }

  /**
   * Reads the source path from the plugin config.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function readSourcePathConfig(): void {
    $source_path = $this->getSetting(
      static::SOURCE_PATH_SETTING,
      static::SOURCE_PATH_REQUIRED
    );
    if ($source_path !== NULL) {
      $this->sourcePathSpec = $this->recordPathParser
        ->parsePathSpec($source_path);
    }
  }

  /**
   * Reads the target path from the plugin config.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function readTargetPathConfig(): void {
    $target_path = $this->getSetting(
      static::TARGET_PATH_SETTING,
      static::TARGET_PATH_REQUIRED
    );
    if ($target_path !== NULL) {
      $this->targetPath = $this->recordPathParser->parsePath($target_path);
    }
  }

  /**
   * Returns source value.
   *
   * @param \Drupal\data_transfer\Import\Record\ImportRecordInterface $record
   *   The import record to read from.
   *
   * @return mixed|null
   *   The source value or NULL if it's not set.
   */
  protected function getSourceValue(
    ImportRecordInterface $record
  ) {
    return $record->getValue($this->sourcePathSpec);
  }

  /**
   * Sets the target value.
   *
   * @param \Drupal\data_transfer\Import\Record\ImportRecordInterface $record
   *   The record to set the value to.
   * @param mixed $value
   *   The value to set.
   */
  protected function setTargetValue(ImportRecordInterface $record, $value): void {
    $record->setTargetValue($this->targetPath, $value);
  }

  /**
   * Checks if plugin has target path to apply value to.
   *
   * @return bool
   *   Target path exists in configuration.
   */
  protected function hasTargetPath(): bool {
    return ($this->targetPath !== NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function processRecords(ImportRecordSetInterface $record_set): void {
    foreach ($record_set as $record) {
      try {
        $this->processRecord($record);
      }
      catch (ExchangeSkipRowException $e) {
        $record->addError($e->getMessage());
      }
    }
  }

  /**
   * Processes a single record of the record set.
   *
   * @param \Drupal\data_transfer\Import\Record\ImportRecordInterface $record
   *   The record to process.
   *
   * @throws \Drupal\data_transfer\Exception\ExchangeSkipRowException
   *   Thrown in case the row isn't valid and should be excluded from further
   *   processing. It actually just adds an error message of the exception to
   *   the row.
   */
  abstract protected function processRecord(
    ImportRecordInterface $record
  ): void;

}
