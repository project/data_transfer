<?php

namespace Drupal\data_transfer\Import\Processor;

use Drupal\data_transfer\Plugin\PluginManagerBase;

/**
 * Plugin manager of the import processors.
 */
class ImportProcessorPluginManager extends PluginManagerBase implements ImportProcessorPluginManagerInterface {

  /**
   * {@inheritdoc}
   */
  public const SUBDIR = 'Plugin/data_transfer/ImportProcessor';

  /**
   * {@inheritdoc}
   */
  public const CACHE_KEY = 'import_processor';

  /**
   * {@inheritdoc}
   */
  public const PLUGIN_INTERFACE = ImportProcessorInterface::class;

}
