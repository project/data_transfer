<?php

namespace Drupal\data_transfer\Import\Processor;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\data_transfer\Import\Record\ImportRecordSetInterface;

/**
 * Represents processor of the source record into the target record.
 *
 * The source record returned by the mapper is passed to all the configured
 * processor plugins. They should usually de-normalize the data without taking
 * into consideration the existing target object.
 */
interface ImportProcessorInterface extends PluginInspectionInterface {

  /**
   * Processes records of the record set.
   *
   * The rows with errors are excluded from further processing and writing.
   *
   * @param \Drupal\data_transfer\Import\Record\ImportRecordSetInterface $record_set
   *   The record set to process.
   */
  public function processRecords(ImportRecordSetInterface $record_set): void;

}
