<?php

namespace Drupal\data_transfer\Import\Reader;

use Drupal\data_transfer\Plugin\PluginManagerBase;

/**
 * Plugin manager of the import readers.
 */
class ImportReaderPluginManager extends PluginManagerBase implements ImportReaderPluginManagerInterface {

  /**
   * {@inheritdoc}
   */
  public const SUBDIR = 'Plugin/data_transfer/ImportReader';

  /**
   * {@inheritdoc}
   */
  public const CACHE_KEY = 'import_reader';

  /**
   * {@inheritdoc}
   */
  public const PLUGIN_INTERFACE = ImportReaderInterface::class;

}
