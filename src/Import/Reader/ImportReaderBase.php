<?php

namespace Drupal\data_transfer\Import\Reader;

use Drupal\data_transfer\Import\Record\ImportRecord;
use Drupal\data_transfer\Import\Record\ImportRecordInterface;
use Drupal\data_transfer\Import\Record\ImportRecordSet;
use Drupal\data_transfer\Import\Record\ImportRecordSetInterface;
use Drupal\data_transfer\Plugin\PluginBase;

/**
 * Provides base class for the import reader.
 */
abstract class ImportReaderBase extends PluginBase implements ImportReaderInterface {

  /**
   * The record class to use when creating new instance.
   */
  public const RECORD_CLASS = ImportRecord::class;

  /**
   * The record set class to use when creating new instance.
   */
  public const RECORD_SET_CLASS = ImportRecordSet::class;

  /**
   * Creates new import record.
   *
   * @param array $source_values
   *   The source values.
   *
   * @return \Drupal\data_transfer\Import\Record\ImportRecordInterface
   *   The import record.
   */
  protected function createRecord(array $source_values): ImportRecordInterface {
    $class = static::RECORD_CLASS;
    return new $class($source_values);
  }

  /**
   * Creates instance of a record set with the passed records.
   *
   * @param \Drupal\data_transfer\Import\Record\ImportRecordInterface[] $records
   *   The list of records. Keys are used for logging, so they must be unique
   *   per whole import.
   *
   * @return \Drupal\data_transfer\Import\Record\ImportRecordSetInterface
   *   The record set.
   */
  protected function createRecordSet(array $records): ImportRecordSetInterface {
    $class = static::RECORD_SET_CLASS;
    return new $class($records);
  }

}
