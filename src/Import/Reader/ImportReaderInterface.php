<?php

namespace Drupal\data_transfer\Import\Reader;

use Drupal\data_transfer\Import\Record\ImportRecordSetInterface;

/**
 * Represents the source data record reader of the import process.
 *
 * The source records are then passed to the mapper.
 */
interface ImportReaderInterface {

  /**
   * Returns total number of records that may be imported.
   *
   * @return int
   *   The number of records.
   */
  public function getTotalCount(): int;

  /**
   * Returns the list of properties available for mapping.
   *
   * @return array
   *   Keys are property keys, values are human-readable labels.
   */
  public function getSourceProperties(): array;

  /**
   * Reads the next N records.
   *
   * @param int|null $limit
   *   The limit or NULL to read all the available records.
   *
   * @return array
   *   The list of source records passed to the mapping.
   */
  public function readNextRecordSet(int $limit = NULL): ImportRecordSetInterface;

}
