<?php

namespace Drupal\data_transfer\Import\Batch;

use Drupal\data_transfer\Import\ImporterBase;
use Drupal\data_transfer\Import\ImportResultInterface;
use Drupal\data_transfer\Import\Logger\ImportLoggerInterface;
use Drupal\data_transfer\Import\Reader\ImportReaderInterface;
use Drupal\data_transfer\Import\Writer\ImportWriterInterface;
use Drupal\data_transfer\Plugin\PluginWithStorageInterface;

/**
 * Provides batch importer for the custom import system.
 *
 * @see \Drupal\data_transfer\Import\Batch\BatchImporterFactoryInterface
 * @see \Drupal\data_transfer\Import\Batch\ImportBatchCallbacksInterface
 * @see \Drupal\data_transfer\Import\Batch\ImportBatchGeneratorInterface
 */
class BatchImporter extends ImporterBase implements BatchImporterInterface {

  /**
   * The batch sandbox key of the total number of records to import.
   */
  public const TOTAL_KEY = 'total';

  /**
   * The batch sandbox key holding the number of processed records.
   */
  public const PROCESSED_KEY = 'processed';

  /**
   * The batch sandbox key holding the reader storage.
   */
  public const READER_STORAGE_KEY = 'reader_storage';

  /**
   * The batch sandbox key holding the writer storage.
   */
  public const WRITER_STORAGE_KEY = 'writer_storage';

  /**
   * The batch sandbox key holding the logger storage.
   */
  public const LOGGER_STORAGE_KEY = 'logger_storage';

  /**
   * The batch sandbox key holding the import result storage.
   */
  public const RESULT_STORAGE_KEY = 'result_storage';

  /**
   * {@inheritdoc}
   */
  public function importRecordSet(array &$context) {
    $sandbox =& $context['sandbox'];
    $reader = $this->getReader($sandbox[static::READER_STORAGE_KEY] ?? []);
    $logger = $this->getLogger($sandbox[static::LOGGER_STORAGE_KEY] ?? []);

    $is_init = empty($sandbox);
    if ($is_init) {
      $sandbox[static::TOTAL_KEY] = $reader->getTotalCount();
      $sandbox[static::PROCESSED_KEY] = 0;
      $sandbox[static::READER_STORAGE_KEY] = [];
      $sandbox[static::WRITER_STORAGE_KEY] = [];
      $sandbox[static::RESULT_STORAGE_KEY] = [];
    }

    $writer = $this->getWriter($sandbox[static::WRITER_STORAGE_KEY]);
    $mapper = $this->getMapper();
    $processors = $this->getProcessors();
    $result = $this->createImportResult($sandbox[static::RESULT_STORAGE_KEY]);

    if ($is_init) {
      $result->setTotal($sandbox[static::TOTAL_KEY]);
      $logger->logImportStart($result);
    }

    $total = $sandbox[static::TOTAL_KEY];
    $processed = $sandbox[static::PROCESSED_KEY];
    $limit = $this->importSpecification->getBatchRecordsLimit();
    $logger->logRecordSetStart($result);

    $record_set = $this->readRecords($reader, $limit, $result, $logger);
    $record_set = $this->mapRecords($mapper, $record_set, $logger);
    $record_set = $this->processRecords($processors, $record_set, $logger);
    $this->writeRecords($writer, $record_set, $result, $logger);

    $processed += $limit;
    $logger->logRecordSetFinish($result);

    if ($processed >= $total) {
      $finished = 1;
    }
    else {
      $finished = min($processed / $total, 0.99);
    }
    $context['finished'] = $finished;

    if ($finished >= 1) {
      $logger->logImportFinish($result);
      return $result->toArray();
    }
    else {
      $sandbox[static::PROCESSED_KEY] = $processed;
      $this->persistStorages($sandbox, $reader, $writer, $logger, $result);
    }
  }

  /**
   * Saves storage of different objects into the batch sandbox.
   *
   * @param array $sandbox
   *   The batch sandbox.
   * @param \Drupal\data_transfer\Import\Reader\ImportReaderInterface $reader
   *   The reader plugin.
   * @param \Drupal\data_transfer\Import\Writer\ImportWriterInterface $writer
   *   The writer plugin.
   * @param \Drupal\data_transfer\Import\Logger\ImportLoggerInterface $logger
   *   The logger plugin.
   * @param \Drupal\data_transfer\Import\ImportResultInterface $result
   *   The import result.
   */
  protected function persistStorages(
    array &$sandbox,
    ImportReaderInterface $reader,
    ImportWriterInterface $writer,
    ImportLoggerInterface $logger,
    ImportResultInterface $result
  ): void {
    $map = [
      static::READER_STORAGE_KEY => $reader,
      static::WRITER_STORAGE_KEY => $writer,
      static::LOGGER_STORAGE_KEY => $logger,
    ];
    foreach ($map as $storage_key => $plugin) {
      if (!$plugin instanceof PluginWithStorageInterface) {
        continue;
      }

      $sandbox[$storage_key] = $plugin->getStorage()
        ->toArray();
    }

    $sandbox[static::RESULT_STORAGE_KEY] = $result->toArray();
  }

}
