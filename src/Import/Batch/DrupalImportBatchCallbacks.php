<?php

namespace Drupal\data_transfer\Import\Batch;

use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Url;
use Drupal\data_transfer\Batch\FinalizingBatchCallbacksInterface;
use Drupal\data_transfer\Import\ImporterBase;
use Drupal\data_transfer\Import\ImportResult;

/**
 * Provides batch callbacks for the import launched from Drupal.
 *
 * It shows statistics and links as Drupal messages.
 */
class DrupalImportBatchCallbacks extends ImportBatchCallbacksBase implements FinalizingBatchCallbacksInterface {

  use MessengerTrait;

  /**
   * The import result class name.
   */
  protected const IMPORT_RESULT_CLASS = ImporterBase::IMPORT_RESULT_CLASS;

  /**
   * The text key in the result link array.
   */
  protected const RESULT_LINK_LABEL_KEY = ImportResult::LINK_LABEL_KEY;

  /**
   * The URL key in the result link array.
   */
  protected const RESULT_LINK_URL_KEY = ImportResult::LINK_URL_KEY;

  /**
   * {@inheritdoc}
   */
  public function finishBatch($results): void {
    $messenger = $this->messenger();

    $result_class = static::IMPORT_RESULT_CLASS;
    foreach ($results as $result_storage) {
      /** @var \Drupal\data_transfer\Import\ImportResultInterface $result */
      $result = new $result_class($result_storage);
      $messenger->addStatus($this->t(
        'Imported @imported out of @total records.',
        [
          '@imported' => $result->getTargetNumber(),
          '@total' => $result->getTotal(),
        ]
      ));
      foreach ($result->getLinks() as $link_data) {
        $link_text = $link_data[static::RESULT_LINK_LABEL_KEY];
        $link_url = $link_data[static::RESULT_LINK_URL_KEY];
        $messenger->addStatus(Link::fromTextAndUrl($link_text, Url::fromUri($link_url)));
      }
    }
  }

}
