<?php

namespace Drupal\data_transfer\Import\Batch;

/**
 * Provides import methods for batch processing.
 */
interface BatchImporterInterface {

  /**
   * Imports a single set of records in the batch processing.
   *
   * @param array $context
   *   The batch context.
   *
   * @return mixed|null
   *   The result data if it's over.
   */
  public function importRecordSet(array &$context);

}
