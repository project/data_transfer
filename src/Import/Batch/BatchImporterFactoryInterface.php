<?php

namespace Drupal\data_transfer\Import\Batch;

use Drupal\data_transfer\Entity\ImportSpecificationInterface;

/**
 * Provides batch importer factory.
 */
interface BatchImporterFactoryInterface {

  /**
   * Creates instance of batch importer for the passed import specification.
   *
   * @param \Drupal\data_transfer\Entity\ImportSpecificationInterface $import_specification
   *   The import specification to follow.
   * @param array $input
   *   The input array to pass to the importer.
   *
   * @return \Drupal\data_transfer\Import\Batch\BatchImporterInterface
   *   The batch importer instance.
   */
  public function createImporter(
    ImportSpecificationInterface $import_specification,
    array $input = []
  ): BatchImporterInterface;

}
