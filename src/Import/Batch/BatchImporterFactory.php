<?php

namespace Drupal\data_transfer\Import\Batch;

use Drupal\data_transfer\ArgumentsPassingFactoryBase;
use Drupal\data_transfer\Entity\ImportSpecificationInterface;

/**
 * Provides batch importer factory.
 *
 * @see \Drupal\data_transfer\Import\Batch\BatchImporter
 */
class BatchImporterFactory extends ArgumentsPassingFactoryBase implements BatchImporterFactoryInterface {

  /**
   * The importer class name.
   */
  public const CLASS_NAME = BatchImporter::class;

  /**
   * {@inheritdoc}
   */
  public function createImporter(
    ImportSpecificationInterface $import_specification,
    array $input = []
  ): BatchImporterInterface {
    $class = static::CLASS_NAME;
    return new $class(
      $import_specification,
      $input,
      ...$this->arguments
    );
  }

}
