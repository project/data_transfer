<?php

namespace Drupal\data_transfer\Import\Batch;

use Drupal\data_transfer\Batch\BatchCallbacksInterface;

/**
 * Interface for the custom import batch callbacks.
 *
 * @see \Drupal\data_transfer\Import\Batch\ImportBatchGeneratorInterface
 */
interface ImportBatchCallbacksInterface extends BatchCallbacksInterface {

  /**
   * The batch operation handler.
   *
   * @param string $spec_id
   *   The export specification ID.
   * @param array $input
   *   The input array.
   * @param array $context
   *   The batch context.
   */
  public function operation(
    string $spec_id,
    array $input,
    array &$context
  ): void;

}
