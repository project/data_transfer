<?php

namespace Drupal\data_transfer\Import\Batch;


use Drupal\data_transfer\Batch\BatchGeneratorBase;

/**
 * The batch generator for import.
 */
class ImportBatchGenerator extends BatchGeneratorBase implements ImportBatchGeneratorInterface {

  /**
   * {@inheritdoc}
   */
  public function setUpBatchProcessing(
    ImportBatchCallbacksInterface $callbacks,
    string $spec_id,
    array $input = []
  ): bool {
    $operations = [];
    $operations[] = $this->buildOperation(
      $callbacks,
      'operation',
      [$spec_id, $input]
    );

    if (empty($operations)) {
      return FALSE;
    }

    $batch = [
      'operations' => $operations,
    ];
    if ($callbacks instanceof FinalizingBatchCallbacksInterface) {
      $batch += [
        'finished' => static::class . '::finishBatch',
      ];
    }
    batch_set($batch);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function startBatchProcessing(
    ImportBatchCallbacksInterface $callbacks,
    string $spec_id,
    array $input = []
  ): ?int {
    if (!$this->setUpBatchProcessing($callbacks, $spec_id, $input)) {
      return NULL;
    }

    return $this->generateBatchId();
  }

}
