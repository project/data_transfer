<?php

namespace Drupal\data_transfer\Import\Batch;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\data_transfer\Batch\BatchCallbacksBase;
use Drupal\data_transfer\Entity\EntityTypes;
use Drupal\data_transfer\Entity\ImportSpecificationInterface;

/**
 * Provides base class for custom import batch callbacks.
 *
 * Child classes should only take care about passing the import results to the
 * user in a way specific to the UI used.
 */
abstract class ImportBatchCallbacksBase extends BatchCallbacksBase implements ImportBatchCallbacksInterface {

  /**
   * The import specification entity type ID.
   */
  protected const SPEC_ENTITY_TYPE = EntityTypes::IMPORT_SPECIFICATION;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The batch importer factory.
   *
   * @var \Drupal\data_transfer\Import\Batch\BatchImporterFactoryInterface
   */
  protected $batchImporterFactory;

  /**
   * A constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    BatchImporterFactoryInterface $batch_importer_factory
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->batchImporterFactory = $batch_importer_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function operation(
    string $spec_id,
    array $input,
    array &$context
  ): void {
    $spec = $this->entityTypeManager->getStorage(static::SPEC_ENTITY_TYPE)
      ->load($spec_id);
    if (!$spec instanceof ImportSpecificationInterface) {
      throw new \InvalidArgumentException(sprintf(
        "The %s import specification doesn't exist.",
        $spec_id
      ));
    }

    $importer = $this->batchImporterFactory->createImporter($spec, $input);
    $result = $importer->importRecordSet($context);
    if ($result !== NULL) {
      $context['results'][$this->getServiceName()][] = $result;
    }
  }

}
