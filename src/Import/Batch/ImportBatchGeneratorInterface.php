<?php

namespace Drupal\data_transfer\Import\Batch;

/**
 * The batch generator for the import.
 *
 * @see \Drupal\data_transfer\Import\Batch\ImportBatchCallbacksInterface
 */
interface ImportBatchGeneratorInterface {

  /**
   * Sets up the batch processing.
   *
   * @param \Drupal\data_transfer\Import\Batch\ImportBatchCallbacksInterface $callbacks
   *   The callbacks service instance.
   * @param string $spec_id
   *   The export specification ID.
   * @param array $input
   *   The input to pass to the exporter.
   *
   * @return bool
   *   TRUE if the batch was set, FALSE in case no queued products exist.
   */
  public function setUpBatchProcessing(
    ImportBatchCallbacksInterface $callbacks,
    string $spec_id,
    array $input = []
  ): bool;

  /**
   * Sets up the batch and generates the batch ID.
   *
   * @param \Drupal\data_transfer\Import\Batch\ImportBatchCallbacksInterface $callbacks
   *   The callbacks service instance.
   * @param string $spec_id
   *   The export specification ID.
   * @param array $input
   *   The input to pass to the exporter.
   *
   * @return int|null
   *   The batch ID or NULL in case no queued products exist.
   */
  public function startBatchProcessing(
    ImportBatchCallbacksInterface $callbacks,
    string $spec_id,
    array $input = []
  ): ?int;

}
