<?php

namespace Drupal\data_transfer\Import\Record;

/**
 * Represents multiple records imported at once.
 */
interface ImportRecordSetInterface extends \IteratorAggregate, \Countable {

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\data_transfer\Import\Record\ImportRecordInterface[]
   *   The records.
   */
  public function getIterator();

  /**
   * Clones the record set with the valid records only.
   *
   * The record is valid in case it has no errors.
   *
   * @param bool $target_to_source
   *   TRUE to clone the records setting their target data to the source data.
   *
   * @return \Drupal\data_transfer\Import\Record\ImportRecordSetInterface
   *   The cloned record set.
   */
  public function cloneWithValidRecords(
    bool $target_to_source = FALSE
  ): ImportRecordSetInterface;

}
