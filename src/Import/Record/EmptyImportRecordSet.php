<?php

namespace Drupal\data_transfer\Import\Record;

/**
 * Represents an empty record set.
 *
 * It is used in case of errors in the normal record set.
 */
class EmptyImportRecordSet implements ImportRecordSetInterface {

  /**
   * {@inheritdoc}
   */
  public function count() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getIterator() {
    return new \EmptyIterator();
  }

  /**
   * {@inheritdoc}
   */
  public function cloneWithValidRecords(
    bool $target_to_source = FALSE
  ): ImportRecordSetInterface {
    return clone $this;
  }

}
