<?php

namespace Drupal\data_transfer\Import\Record;

/**
 * Represents multiple records imported at once.
 */
class ImportRecordSet implements ImportRecordSetInterface {

  /**
   * The records.
   *
   * @var \Drupal\data_transfer\Import\Record\ImportRecordInterface[]
   */
  protected $records;

  /**
   * A constructor.
   *
   * @param \Drupal\data_transfer\Import\Record\ImportRecordInterface[] $records
   *   The records.
   */
  public function __construct(array $records) {
    $this->records = $records;
  }

  /**
   * {@inheritdoc}
   */
  public function getIterator() {
    return new \ArrayIterator($this->records);
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    return count($this->records);
  }

  /**
   * {@inheritdoc}
   */
  public function cloneWithValidRecords(
    bool $target_to_source = FALSE
  ): ImportRecordSetInterface {
    $records = [];
    foreach ($this->records as $key => $record) {
      if ($record->hasErrors()) {
        continue;
      }

      if ($target_to_source) {
        $record = $record->cloneTargetToSource();
      }

      $records[$key] = $record;
    }

    $clone = clone $this;
    $clone->records = $records;
    return $clone;
  }

}
