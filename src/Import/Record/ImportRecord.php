<?php

namespace Drupal\data_transfer\Import\Record;

use Drupal\Component\Utility\NestedArray;
use Drupal\data_transfer\ErrorListTrait;
use Drupal\data_transfer\Exchange\RecordPath\RecordPathSpecInterface;
use Drupal\data_transfer\Utility\NestedData;

/**
 * Represents a single import record with source and target data.
 */
class ImportRecord implements ImportRecordInterface {

  use ErrorListTrait;

  /**
   * The source data.
   *
   * @var array
   */
  protected $source;

  /**
   * The target data.
   *
   * @var array
   */
  protected $target = [];

  /**
   * A constructor.
   *
   * @param array $source
   *   The source data.
   */
  public function __construct(array $source) {
    $this->source = $source;
  }

  /**
   * {@inheritdoc}
   */
  public function getSource() {
    return $this->source;
  }

  /**
   * {@inheritdoc}
   */
  public function &getTarget(): array {
    return $this->target;
  }

  /**
   * {@inheritdoc}
   */
  public function setTarget(array $target): void {
    $this->target = $target;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceValue(array $path) {
    return NestedData::getValue($this->source, $path);
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetValue(array $path) {
    return NestedData::getValue($this->target, $path);
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(RecordPathSpecInterface $path_spec) {
    if ($path_spec->isFromTarget()) {
      return $this->getTargetValue($path_spec->getPath());
    }
    else {
      return $this->getSourceValue($path_spec->getPath());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetValue(array $path, $value): void {
    NestedArray::setValue($this->target, $path, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function cloneTargetToSource(): ImportRecordInterface {
    $clone = clone $this;

    // Move the target to the clone source and wipe its target.
    $clone->source = $this->target;
    $clone->target = [];

    // Also wipe errors, because they aren't relevant any longer.
    $clone->errors = [];

    return $clone;
  }

}
