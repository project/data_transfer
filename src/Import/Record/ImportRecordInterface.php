<?php

namespace Drupal\data_transfer\Import\Record;

use Drupal\data_transfer\ErrorListInterface;
use Drupal\data_transfer\Exchange\RecordPath\RecordPathSpecInterface;

/**
 * Represents a single import record with source and target data.
 */
interface ImportRecordInterface extends ErrorListInterface {

  /**
   * Returns source data.
   *
   * @return array
   *   The source data.
   */
  public function getSource();

  /**
   * Returns the target data by reference.
   *
   * @return array
   *   The target data, returned by reference.
   */
  public function &getTarget(): array;

  /**
   * Returns the source data value.
   *
   * @param array $path
   *   The path to the value.
   *
   * @return mixed
   *   The value or NULL if it's not set.
   */
  public function getSourceValue(array $path);

  /**
   * Returns the target data value.
   *
   * @param array $path
   *   The path to the value.
   *
   * @return mixed
   *   The value or NULL if it's not set.
   */
  public function getTargetValue(array $path);

  /**
   * Returns data value either from source or from target data.
   *
   * @param \Drupal\data_transfer\Exchange\RecordPath\RecordPathSpecInterface $path_spec
   *   The path specification.
   *
   * @return mixed
   *   The value or NULL if it doesn't exist.
   */
  public function getValue(RecordPathSpecInterface $path_spec);

  /**
   * Sets the target data value.
   *
   * @param array $path
   *   The path to the value.
   * @param mixed $value
   *   The value to set.
   */
  public function setTargetValue(array $path, $value): void;

  /**
   * Sets the target data.
   *
   * @param array $target
   *   The target data to set.
   */
  public function setTarget(array $target): void;

  /**
   * Clones the record moving the target data to the source one.
   *
   * The target data is empty on the cloned record.
   *
   * @return static
   *   The cloned record.
   */
  public function cloneTargetToSource(): ImportRecordInterface;

}
