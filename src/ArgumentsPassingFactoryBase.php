<?php

namespace Drupal\data_transfer;

/**
 * Provides base class for a factory that passes its arguments to the instance.
 */
abstract class ArgumentsPassingFactoryBase {

  /**
   * The arguments to pass to a constructor of the created object.
   *
   * @var array
   */
  protected $arguments = [];

  /**
   * A constructor.
   *
   * @param mixed $args
   *   The arguments are passed to a constructor of the created object.
   */
  public function __construct(...$args) {
    $this->arguments = $args;
  }

}
