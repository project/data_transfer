<?php

namespace Drupal\data_transfer\Plugin;

/**
 * Interface of a plugin that uses persistent storage.
 *
 * @see \Drupal\data_transfer\Plugin\PluginStorageInterface
 * @see \Drupal\data_transfer\Plugin\PluginWithStorageTrait
 */
interface PluginWithStorageInterface {

  /**
   * Sets the plugin storage.
   *
   * @param \Drupal\data_transfer\Plugin\PluginStorageInterface $storage
   *   The storage to set.
   *
   * @return static
   *   Self.
   */
  public function setStorage(
    PluginStorageInterface $storage
  ): PluginWithStorageInterface;

  /**
   * Returns the plugin storage.
   *
   * @return \Drupal\data_transfer\Plugin\PluginStorageInterface
   *   The storage.
   */
  public function getStorage(): PluginStorageInterface;

}
