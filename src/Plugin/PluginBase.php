<?php

namespace Drupal\data_transfer\Plugin;

use Drupal\Core\Plugin\PluginBase as CorePluginBase;
use Drupal\data_transfer\Exception\PluginConfigurationException;
use Drupal\data_transfer\Export\PluginConfigurationKeys;

/**
 * Provides base class for the import/export plugins.
 */
class PluginBase extends CorePluginBase {

  /**
   * The input key in the plugin configuration.
   */
  protected const INPUT_KEY = PluginConfigurationKeys::INPUT;

  /**
   * Returns setting value.
   *
   * @param string $key
   *   The setting key.
   * @param bool $is_required
   *   TRUE to require a value.
   *
   * @return mixed|null
   *   The setting value or NULL if it's not set and isn't required.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   *   Thrown in case required setting is not set.
   */
  protected function getSetting(string $key, bool $is_required = TRUE) {
    $value = $this->configuration[$key] ?? NULL;
    if (!isset($value) && $is_required) {
      throw new PluginConfigurationException(sprintf(
        'The %s setting is required for the %s plugin.',
        $key,
        $this->getPluginId()
      ));
    }

    return $value;
  }

  /**
   * Returns value from the input passed to the exporter.
   *
   * @param string $key
   *   The input key.
   *
   * @return mixed|null
   *   The input value or NULL if it wasn't provided.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function getInputValue(string $key) {
    $input = $this->getSetting(static::INPUT_KEY, FALSE) ?? [];
    return $input[$key] ?? NULL;
  }

}
