<?php

namespace Drupal\data_transfer\Plugin;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\data_transfer\Export\PluginConfigurationKeys;

/**
 * Provides base class for the plugin managers.
 */
abstract class PluginManagerBase extends DefaultPluginManager {

  /**
   * The plugin sub-folder in the module.
   *
   * Should be specified in child class.
   */
  protected const SUBDIR = NULL;

  /**
   * The plugin annotation class name.
   */
  public const PLUGIN_ANNOTATION_NAME = Plugin::class;

  /**
   * The cache key.
   *
   * Should be specified in child class.
   */
  protected const CACHE_KEY = NULL;

  /**
   * The plugin interface.
   *
   * Should be specified in child class.
   */
  protected const PLUGIN_INTERFACE = NULL;

  /**
   * The input key in the plugin configuration.
   */
  protected const INPUT_KEY = PluginConfigurationKeys::INPUT;

  /**
   * A constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      static::SUBDIR,
      $namespaces,
      $module_handler,
      static::PLUGIN_INTERFACE,
      static::PLUGIN_ANNOTATION_NAME
    );

    $this->setCacheBackend($cache_backend, static::CACHE_KEY);
  }

  /**
   * Creates plugin instance from its specification.
   *
   * @param \Drupal\data_transfer\Plugin\PluginSpecificationInterface $specification
   *   The plugin specification.
   * @param array $input
   *   The input to add to the plugin configuration.
   *
   * @return object
   *   The plugin instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function createFromSpecification(
    PluginSpecificationInterface $specification,
    array $input = []
  ) {
    $configuration = $specification->getConfiguration();
    $configuration[static::INPUT_KEY] = $input;

    $instance = $this->createInstance(
      $specification->getPluginId(),
      $configuration
    );

    if ($instance instanceof ConfigurationReadingPluginInterface) {
      $instance->readConfiguration();
    }

    return $instance;
  }

}
