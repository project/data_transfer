<?php

namespace Drupal\data_transfer\Plugin;

use Drupal\Component\Utility\NestedArray;

/**
 * The persistent plugin storage.
 *
 * @see \Drupal\data_transfer\Plugin\PluginStorageInterface
 */
class PluginStorage implements PluginStorageInterface {

  /**
   * The actual data storage.
   *
   * @var array
   */
  protected $storage = [];

  /**
   * A constructor.
   *
   * @param array $storage
   *   The storage to initialize with. Should be the same as returned by the
   *   ::toArray() method.
   *
   * @see toArray()
   */
  public function __construct(array $storage = []) {
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  public function get($key) {
    return NestedArray::getValue($this->storage, (array) $key);
  }

  /**
   * {@inheritdoc}
   */
  public function set($key, $value): PluginStorageInterface {
    NestedArray::setValue($this->storage, (array) $key, $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isset($key): bool {
    $key_exists = FALSE;
    NestedArray::getValue($this->storage, (array) $key, $key_exists);
    return $key_exists;
  }

  /**
   * {@inheritdoc}
   */
  public function unset($key): PluginStorageInterface {
    NestedArray::unsetValue($this->storage, (array) $key);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    return empty($this->storage);
  }

  /**
   * {@inheritdoc}
   */
  public function toArray(): array {
    return $this->storage;
  }

}
