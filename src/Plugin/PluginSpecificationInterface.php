<?php

namespace Drupal\data_transfer\Plugin;

/**
 * Represents plugin specification.
 *
 * It contains all the necessary information to create a plugin instance.
 */
interface PluginSpecificationInterface {

  /**
   * Returns the plugin ID.
   *
   * @return string
   *   The plugin ID.
   */
  public function getPluginId(): string;

  /**
   * The plugin configuration.
   *
   * @return array
   *   The configuration.
   */
  public function getConfiguration(): array;

}
