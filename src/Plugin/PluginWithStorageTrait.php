<?php

namespace Drupal\data_transfer\Plugin;

/**
 * Provides trait for a plugin that uses persistent storage.
 *
 * @see \Drupal\data_transfer\Plugin\PluginWithStorageInterface
 */
trait PluginWithStorageTrait {

  /**
   * The storage.
   *
   * @var \Drupal\data_transfer\Plugin\PluginStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  public function setStorage(
    PluginStorageInterface $storage
  ): PluginWithStorageInterface {
    $this->storage = $storage;
    return $this;
  }

  /**
   * Creates an instance of the storage.
   *
   * @return \Drupal\data_transfer\Plugin\PluginStorageInterface
   *   The storage created.
   */
  protected function createStorage(): PluginStorageInterface {
    return new PluginStorage();
  }

  /**
   * {@inheritdoc}
   */
  public function getStorage(): PluginStorageInterface {
    if (!isset($this->storage)) {
      $this->storage = $this->createStorage();
    }

    return $this->storage;
  }

}
