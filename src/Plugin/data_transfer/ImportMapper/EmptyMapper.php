<?php

namespace Drupal\data_transfer\Plugin\data_transfer\ImportMapper;

use Drupal\data_transfer\Import\Mapper\ImportMapperBase;
use Drupal\data_transfer\Import\Record\ImportRecordInterface;

/**
 * Provides empty mapper for 'data_transfer' import.
 *
 * It won't do any mappings, just returns source record as is.
 *
 * @Plugin(
 *   id = "empty",
 * )
 */
class EmptyMapper extends ImportMapperBase {

  /**
   * {@inheritdoc}
   */
  protected function mapRecord(ImportRecordInterface $record): void {
    $record->setTarget($record->getSource());
  }

}
