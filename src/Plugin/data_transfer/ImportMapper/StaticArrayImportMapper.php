<?php

namespace Drupal\data_transfer\Plugin\data_transfer\ImportMapper;

use Drupal\data_transfer\Import\Mapper\ImportMapperBase;
use Drupal\data_transfer\Import\Record\ImportRecordInterface;

/**
 * The mapper that uses static list of properties and maps using the order only.
 *
 * It takes values of the source record (ignoring keys) and assigns those
 * entries to the specified list of properties.
 *
 * Available configuration options:
 * - properties: the list of keys given to the properties found in the source
 *   record.
 * - allow_missing: (optional) whether to allow missing properties or not,
 *   defaults to FALSE. When set to TRUE, missing properties are set to NULL.
 * - missing_properties_error: (optional) the error to log for the records with
 *   not enough properties, if that's not allowed.
 *
 * @Plugin(
 *   id = "static_array",
 * )
 */
class StaticArrayImportMapper extends ImportMapperBase {

  /**
   * The setting key that holds the list of properties to map to.
   */
  public const PROPERTIES_SETTING = 'properties';

  /**
   * The boolean setting key that controls if the mapper allows missing props.
   */
  public const ALLOW_MISSING_SETTING = 'allow_missing';

  /**
   * The setting key that holds the error message for the missing properties.
   */
  public const MISSING_ERROR_SETTING = 'missing_properties_error';

  /**
   * The list of record properties to map the source data to.
   *
   * @var array
   */
  protected $properties;

  /**
   * Whether to allow missing properties in the source record or not.
   *
   * @var bool
   */
  protected $allowMissing;

  /**
   * The error for the missing properties, if they aren't allowed.
   *
   * @var string
   */
  protected $missingPropertiesError;

  /**
   * A constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->readConfig();
  }

  /**
   * Reads plugin configuration values to its properties for fast access.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function readConfig(): void {
    $this->properties = array_values(
      $this->getSetting(static::PROPERTIES_SETTING)
    );
    $this->allowMissing = $this->getSetting(static::ALLOW_MISSING_SETTING, FALSE)
      ?? FALSE;
    $this->missingPropertiesError = $this
      ->getSetting(static::MISSING_ERROR_SETTING, FALSE)
      ?? '';
  }

  /**
   * {@inheritdoc}
   */
  protected function mapRecord(ImportRecordInterface $record): void {
    $source = array_values($record->getSource());

    if (!$this->allowMissing && count($source) !== count($this->properties)) {
      $record->addError($this->missingPropertiesError);
      return;
    }

    foreach ($this->properties as $index => $key) {
      $value = $source[$index] ?? NULL;
      $record->setTargetValue([$key], $value);
    }
  }

}
