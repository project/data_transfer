<?php

namespace Drupal\data_transfer\Plugin\data_transfer\ExportTransformer;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\data_transfer\Exception\PluginConfigurationException;
use Drupal\data_transfer\Export\Transformer\ExportTransformerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds referenced entities to the source records.
 *
 * Available configuration options:
 * - from: path of the source entity. In order to use source entity from a
 *   nested list, use ".*." as separator. For example, "items.*.item" path
 *   means that the transformer adds related entities to every list of "items"
 *   taking source entity from the "item" key in the sub-record. The depth is
 *   not limited.
 * - reference_field: the reference field name on the source entity to extract
 *   the referenced entity IDs from. The referenced entity type ID is also
 *   detected from this field definition.
 * - merge_type: the way referenced entities are merged back into records. Types
 *   supported:
 *   - combine: source records are combined with the referenced entities. In
 *     case there are N referenced entities, the source record is duplicated N
 *     times to combine with every referenced entity. In case there are no
 *     referenced entities, source record is left as-is.
 *   - nest: referenced entities are added as an array to the source record.
 *     Every referenced entity is wrapped into an array.
 * - to: the property in the source record to put the result into.
 * - wrap_into: the property key of the referenced entity in the wrapper array
 *   when using "nest" merge type.
 *
 * @Plugin(
 *   id = "referenced_entity",
 * )
 */
class ReferencedEntityExportTransformer extends ExportTransformerBase implements ContainerFactoryPluginInterface {

  /**
   * The referenced field name setting.
   */
  public const REFERENCE_FIELD_SETTING = 'reference_field';

  /**
   * The target property setting.
   */
  public const TARGET_KEY_SETTING = 'to';

  /**
   * The setting name of the property to wrap the referenced entity into.
   *
   * Only affects the "nest" merge type.
   */
  public const NESTED_ENTITY_KEY_SETTING = 'wrap_into';

  /**
   * The merge type setting.
   */
  public const MERGE_TYPE_SETTING = 'merge_type';

  /**
   * The map of merge types to the corresponding class method.
   *
   * @see combineEntities()
   * @see nestEntities()
   */
  public const MERGE_CALLBACKS = [
    'combine' => 'combineEntities',
    'nest' => 'nestEntities',
  ];

  /**
   * The key separator to use when combining records with referenced entities.
   */
  public const COMBINED_KEY_SEPARATOR = ':';

  /**
   * The key separator to use when building full key of the nested record.
   */
  public const NESTED_KEY_SEPARATOR = '.';

  /**
   * The entity reference field settings reader.
   *
   * @var \Drupal\data_transfer\Field\EntityReference\EntityReferenceSettingsReaderInterface
   */
  protected $entityReferenceSettingsReader;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityReferenceSettingsReader = $container->get(
      'data_transfer.field.entity_reference.settings_reader'
    );

    return $instance;
  }

  /**
   * Returns information about referenced entities in the export data.
   *
   * @param string[] $path
   *   The path to the source entities.
   * @param array $data
   *   The export data.
   *
   * @return array
   *   The array with:
   *   - flat list of referenced entity IDs.
   *   - map of the full record key to the array of referenced entity IDs as
   *     keys and TRUE as values.
   *   - the referenced entity type ID.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function getReferencedEntitiesInfo(
    array $path,
    array $data
  ): array {
    $ids = [];
    $key_to_ids_map = [];
    $entity_type_id = NULL;
    $reference_field_name = $this->getSetting(static::REFERENCE_FIELD_SETTING);

    foreach ($this->iterateOverSourceRecordSets($path, $data) as $item) {
      list($records, $parent_path, $source_entity_key) = $item;

      foreach ($records as $record_key => $record) {
        $source_entity = $record[$source_entity_key] ?? NULL;
        if (!$source_entity instanceof FieldableEntityInterface) {
          continue;
        }

        $full_record_key = $parent_path;
        $full_record_key[] = $record_key;
        $full_record_key = implode(static::NESTED_KEY_SEPARATOR, $full_record_key);

        $item_list = $source_entity->get($reference_field_name);
        $target_ids = array_column($item_list->getValue(), 'target_id');
        $target_ids_map = array_fill_keys($target_ids, TRUE);
        $ids += $target_ids_map;
        $key_to_ids_map[$full_record_key] = $target_ids_map;

        if (!isset($entity_type_id)) {
          $entity_type_id = $this->entityReferenceSettingsReader
            ->getTargetEntityTypeId($item_list->getFieldDefinition());
        }
      }
    }

    return [
      $ids,
      $key_to_ids_map,
      $entity_type_id,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transform(array &$data): void {
    $path = $this->getSourcePath();

    list($ids, $key_to_ids_map, $entity_type_id) = $this
      ->getReferencedEntitiesInfo($path, $data);

    $entities = $this->loadReferencedEntities($entity_type_id, $ids);
    if (empty($entities)) {
      return;
    }

    $this->mergeEntitiesToData($path, $key_to_ids_map, $entities, $data);
  }

  /**
   * Loads referenced entity IDs.
   *
   * @param string|null $entity_type_id
   *   The referenced entity type ID. In case detection fails and NULL is
   *   passed, an exception is thrown.
   * @param array $ids
   *   The list of referenced entity IDs to load.
   *
   * @return array
   *   The list of loaded entities keyed by their IDs.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function loadReferencedEntities(
    ?string $entity_type_id,
    array $ids
  ): array {
    if (empty($ids)) {
      return [];
    }
    if (!isset($entity_type_id)) {
      throw new \LogicException(sprintf(
        'Unable to detect the referenced entity type ID on the %s field of the %s',
        $this->getSetting(static::REFERENCE_FIELD_SETTING),
        $this->getSetting(static::SOURCE_PATH_SETTING)
      ));
    }

    return $this->entityTypeManager
      ->getStorage($entity_type_id)
      ->loadMultiple(array_keys($ids));
  }

  /**
   * Merges referenced entities to export data.
   *
   * @param array $path
   *   The source entity path in the export data.
   * @param array $key_to_ids_map
   *   The map of full record key to the array with referenced entity IDs as
   *   keys and TRUE as values.
   * @param array $entities
   *   The list of loaded referenced entities to merge into data.
   * @param array $data
   *   The export data to merge the entities to.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function mergeEntitiesToData(
    array $path,
    array $key_to_ids_map,
    array $entities,
    array &$data
  ): void {
    $merge_type = $this->getSetting(static::MERGE_TYPE_SETTING);
    $merge_callback = static::MERGE_CALLBACKS[$merge_type] ?? NULL;
    if (!isset($merge_callback)) {
      throw new PluginConfigurationException(sprintf(
        'The %s merge type is not supported.',
        $merge_type
      ));
    }

    foreach ($this->iterateOverSourceRecordSets($path, $data) as $item) {
      // @todo Use list() once we upgrade to PHP 7.3.
      $records =& $item[0];
      $parent_path = $item[1];

      $this->{$merge_callback}(
        $key_to_ids_map,
        $entities,
        $parent_path,
        $records
      );
    }
  }

  /**
   * Combines the referenced entities with the export data records.
   *
   * @param array $key_to_ids_map
   *   The map of full record key to the array with referenced entity IDs as
   *   keys and TRUE as values.
   * @param array $entities
   *   The list of loaded referenced entities to merge into data.
   * @param array $parent_path
   *   The parent path of the record set.
   * @param array $result
   *   The records to merge the entities into.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function combineEntities(
    array $key_to_ids_map,
    array $entities,
    array $parent_path,
    array &$result
  ): void {
    $target_entity_key = $this->getSetting(static::TARGET_KEY_SETTING);

    $source = $result;
    $result = [];
    foreach ($source as $record_key => $record) {
      $result_key_prefix = $record_key . static::COMBINED_KEY_SEPARATOR;

      $full_record_key = $parent_path;
      $full_record_key[] = $record_key;
      $full_record_key = implode(static::NESTED_KEY_SEPARATOR, $full_record_key);

      $entity_ids_map = $key_to_ids_map[$full_record_key] ?? [];
      $record_entities = array_intersect_key($entities, $entity_ids_map);
      if (empty($record_entities)) {
        $result[$result_key_prefix . '_'] = $source[$record_key];
        continue;
      }

      foreach (array_keys($entity_ids_map) as $index => $entity_id) {
        $entity = $record_entities[$entity_id] ?? NULL;
        if (!isset($entity)) {
          continue;
        }

        $result_record = $record;
        $result_record[$target_entity_key] = $entity;
        $result[$result_key_prefix . $index] = $result_record;
      }
    }
  }

  /**
   * Adds the referenced entities as nested records to the export data records.
   *
   * @param array $key_to_ids_map
   *   The map of full record key to the array with referenced entity IDs as
   *   keys and TRUE as values.
   * @param array $entities
   *   The list of loaded referenced entities to merge into data.
   * @param array $parent_path
   *   The parent path of the record set.
   * @param array $result
   *   The records to merge the entities into.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function nestEntities(
    array $key_to_ids_map,
    array $entities,
    array $parent_path,
    array &$result
  ): void {
    $target_key = $this->getSetting(static::TARGET_KEY_SETTING);
    $nested_key = $this->getSetting(static::NESTED_ENTITY_KEY_SETTING);

    foreach ($result as $record_key => &$record) {
      $full_record_key = $parent_path;
      $full_record_key[] = $record_key;
      $full_record_key = implode(static::NESTED_KEY_SEPARATOR, $full_record_key);

      $entity_ids_map = $key_to_ids_map[$full_record_key] ?? [];
      $record_entities = array_intersect_key($entities, $entity_ids_map);

      $subrecords = [];
      foreach (array_keys($entity_ids_map) as $index => $entity_id) {
        $entity = $record_entities[$entity_id] ?? NULL;
        if (!isset($entity)) {
          continue;
        }

        $subrecords[$index][$nested_key] = $entity;
      }
      $record[$target_key] = $subrecords;
    }
  }

}
