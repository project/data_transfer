<?php

namespace Drupal\data_transfer\Plugin\data_transfer\ImportLogger;

use Drupal\Core\File\Exception\DirectoryNotReadyException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\data_transfer\Entity\EntityTypes;
use Drupal\file\FileInterface;
use Drupal\data_transfer\Import\ImportResultInterface;
use Drupal\data_transfer\Import\Logger\ImportLoggerBase;
use Drupal\data_transfer\Plugin\PluginWithStorageInterface;
use Drupal\data_transfer\Plugin\PluginWithStorageTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides import logger that saves the log to a text file.
 *
 * The list of configuration options:
 * - indentation: (optional) the indentation to use for every error (both
 *   generic and row ones), defaults to " * ".
 * - generic_error_prefix: (optional) the prefix that is added before every
 *   generic error after the indentation, defaults to empty string.
 * - row_error_prefix: (optional) the prefix without indentation that is added
 *   before the row errors, if any. The "[row]" placeholder is replaced with the
 *   row key.
 * - folder: the folder URI to create the log file in.
 * - filename: (optional) the filename template, defaults to
 *   "log-[YYYY-MM-DD].txt", see placeholder provider for the list of available
 *   placeholders.
 * - link_text: the log link text when added to the result.
 *
 * @Plugin(
 *   id = "text_file",
 * )
 */
class TextFileImportLogger extends ImportLoggerBase implements PluginWithStorageInterface, ContainerFactoryPluginInterface {

  use PluginWithStorageTrait;

  /**
   * The storage key that holds the temporary log file URI.
   */
  public const TEMP_FILE_STORAGE_KEY = 'temp_file';

  /**
   * The folder URI to use for temporary file.
   */
  public const TEMP_FOLDER = 'temporary://';

  /**
   * The setting key that holds the indentation string.
   */
  public const INDENTATION_SETTING = 'indentation';

  /**
   * The default indentation string.
   */
  public const DEFAULT_INDENTATION = ' * ';

  /**
   * The line ending.
   */
  public const LINE_ENDING = "\r\n";

  /**
   * The setting key that holds the row error prefix template.
   */
  public const ROW_ERROR_PREFIX_SETTING = 'row_error_prefix';

  /**
   * Default row error prefix template.
   */
  public const DEFAULT_ROW_ERROR_PREFIX = '[row]: ';

  /**
   * The row key placeholder.
   */
  public const ROW_KEY_PLACEHOLDER = '[row]';

  /**
   * The setting key that holds the generic error prefix.
   */
  public const GENERIC_ERROR_PREFIX_SETTING = 'generic_error_prefix';

  /**
   * Default generic error prefix.
   */
  public const DEFAULT_GENERIC_ERROR_PREFIX = '';

  /**
   * The setting key that holds the folder URI for the final log.
   */
  public const FOLDER_SETTING = 'folder';

  /**
   * Flags to use when preparing a folder for writing.
   */
  public const PREPARE_FOLDER_FLAGS = FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS;

  /**
   * The behavior constant to use for the file move.
   */
  protected const FILE_MOVE_BEHAVIOR = FileSystemInterface::EXISTS_RENAME;

  /**
   * The setting key that holds the log filename template.
   */
  public const FILENAME_SETTING = 'filename';

  /**
   * Default log filename template.
   */
  public const DEFAULT_FILENAME = 'log-[YYYY-MM-DD].txt';

  /**
   * The file entity type ID.
   */
  protected const FILE_ENTITY_TYPE = EntityTypes::FILE;

  /**
   * The setting key that holds the log link text when added to the result.
   */
  public const LOG_LINK_TEXT_SETTING = 'link_text';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The filesystem.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The system logger to use as a last resort.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $systemLogger;

  /**
   * The placeholder provider.
   *
   * @var \Drupal\data_transfer\Placeholder\PlaceholderProviderInterface
   */
  protected $placeholderProvider;

  /**
   * The generic errors buffer.
   *
   * @var array
   */
  protected $genericErrors = [];

  /**
   * The row errors buffer grouped by the row key.
   *
   * @var array
   */
  protected $rowErrors = [];

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->currentUser = $container->get('current_user');
    $instance->fileSystem = $container->get('file_system');
    $instance->systemLogger = $container->get('logger.channel.data_transfer');
    $instance->placeholderProvider = $container->get(
      'data_transfer.default_placeholder_provider'
    );

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function logGenericError(string $error): void {
    if ($error === '') {
      return;
    }

    $this->genericErrors[] = $error;
  }

  /**
   * {@inheritdoc}
   */
  public function logRowError(
    string $key,
    string $error
  ): void {
    if ($error === '') {
      return;
    }

    $this->rowErrors[$key][] = $error;
  }

  /**
   * {@inheritdoc}
   */
  public function logRecordSetFinish(ImportResultInterface $result): void {
    // Use this hook to write all the errors from buffers to the temporary file.
    $this->writeErrorBuffersToTempFile();
  }

  /**
   * {@inheritdoc}
   */
  public function logImportFinish(
    ImportResultInterface $result
  ): void {
    // Use this hook to move the log to final location and add it to the result.
    $uri = $this->moveTempFileToDestination();
    if ($uri === NULL) {
      return;
    }

    $file = $this->createManagedFile($uri);
    $this->addLogLinkToResult($file, $result);
  }

  /**
   * Returns URI of the temporary log file.
   *
   * @param bool $create
   *   Pass FALSE to return NULL in case the file haven't been created yet.
   *
   * @return string|null
   *   The file URI. NULL in case the file doesn't exist and shouldn't be
   *   created.
   */
  protected function getTempFileUri(bool $create = TRUE): ?string {
    $storage = $this->getStorage();
    $storage_key = static::TEMP_FILE_STORAGE_KEY;

    $uri = $storage->get($storage_key);
    if ($uri === NULL && $create) {
      $temp_folder = $this->prepareFolder(static::TEMP_FOLDER);
      $uri = $this->fileSystem->tempnam($temp_folder, 'imp');
      $storage->set($storage_key, $uri);
    }

    return $uri;
  }

  /**
   * Prepares the folder for writing.
   *
   * @param string $folder
   *   The folder.
   *
   * @return string
   *   The folder URI.
   *
   * @throws \Drupal\Core\File\Exception\DirectoryNotReadyException
   */
  protected function prepareFolder(string $folder): string {
    $is_ready = $this->fileSystem->prepareDirectory(
      $folder,
      static::PREPARE_FOLDER_FLAGS
    );
    if (!$is_ready) {
      throw new DirectoryNotReadyException(sprintf(
        "The '%s' directory is not writable or doesn't exist.",
        $folder
      ));
    }

    return $folder;
  }

  /**
   * Writes errors from the buffer to the temporary file.
   */
  protected function writeErrorBuffersToTempFile(): void {
    $lines = array_merge(
      $this->formatGenericErrors(),
      $this->formatRowErrors()
    );
    if (empty($lines)) {
      return;
    }

    $uri = $this->getTempFileUri();
    $this->writeLinesToFile($uri, $lines);

    $this->resetErrorBuffers();
  }

  /**
   * Formats row errors for the log file.
   *
   * @return array
   *   The list of lines to add to the log file.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function formatRowErrors(): array {
    if (empty($this->rowErrors)) {
      return [];
    }

    $prefix_template = $this->getSetting(static::ROW_ERROR_PREFIX_SETTING, FALSE)
      ?? static::DEFAULT_ROW_ERROR_PREFIX;
    $indentation = $this->getSetting(static::INDENTATION_SETTING, FALSE)
      ?? static::DEFAULT_INDENTATION;

    $lines = [];
    foreach ($this->rowErrors as $key => $row_errors) {
      $prefix = $indentation . strtr($prefix_template, [
        static::ROW_KEY_PLACEHOLDER => $key,
      ]);

      foreach ($row_errors as $error) {
        $lines[] = $prefix . $error;
      }
    }
    return $lines;
  }

  /**
   * Formats generic errors for the log file.
   *
   * @return array
   *   The list of lines to add to the log file.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function formatGenericErrors(): array {
    if (empty($this->genericErrors)) {
      return [];
    }

    $prefix = $this->getSetting(static::GENERIC_ERROR_PREFIX_SETTING, FALSE)
      ?? static::DEFAULT_GENERIC_ERROR_PREFIX;
    $indentation = $this->getSetting(static::INDENTATION_SETTING, FALSE)
      ?? static::DEFAULT_INDENTATION;

    $lines = [];
    foreach ($this->genericErrors as $error) {
      $lines[] = $indentation . $prefix . $error;
    }
    return $lines;
  }

  /**
   * Wipes the error buffers.
   */
  protected function resetErrorBuffers(): void {
    $this->genericErrors = [];
    $this->rowErrors = [];
  }

  /**
   * Writes the lines to the file at the specified URI.
   *
   * @param string $uri
   *   The file URI.
   * @param array $lines
   *   The list of lines to write to the file.
   */
  protected function writeLinesToFile(string $uri, array $lines): void {
    $file = fopen($uri, 'ab');
    if (!$file) {
      $this->systemLogger->error('Unable to open the file at %uri for writing.', [
        '%uri' => $uri,
      ]);
      return;
    }

    foreach ($lines as $line) {
      $result = fwrite($file, $line . static::LINE_ENDING);
      if ($result === FALSE) {
        $this->systemLogger->error(
          'Unable to write the following error to the %uri log file: @message',
          [
            '%uri' => $uri,
            '@message' => $line,
          ]
        );
      }
    }

    $closed = fclose($file);
    if (!$closed) {
      $this->systemLogger->error('Unable to close the file at %uri for writing.', [
        '%uri' => $uri,
      ]);
      return;
    }
  }

  /**
   * Builds the log file URI.
   *
   * That's the URI in permanent storage, not a temporary one.
   *
   * @return string
   *   The log file URI.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function buildLogFileUri(): string {
    $folder = $this->getSetting(static::FOLDER_SETTING);
    $folder = $this->prepareFolder($folder);

    $filename = $this->getSetting(static::FILENAME_SETTING, FALSE)
      ?? static::DEFAULT_FILENAME;
    $placeholder_values = $this->placeholderProvider->getPlaceholderValues();
    $filename = strtr($filename, $placeholder_values);

    return $folder . '/' . $filename;
  }

  /**
   * Moves the temporary log file to a permanent location.
   *
   * @return string|null
   *   The final log URI. NULL in case there were no errors in the log and it
   *   wasn't create at all.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function moveTempFileToDestination(): ?string {
    $temp_uri = $this->getTempFileUri(FALSE);
    if ($temp_uri === NULL) {
      return NULL;
    }

    $log_uri = $this->buildLogFileUri();
    $log_uri = $this->fileSystem
      ->move($temp_uri, $log_uri, static::FILE_MOVE_BEHAVIOR);

    return $log_uri;
  }

  /**
   * Creates temporary managed file for the final log URI.
   *
   * This is mostly necessary to use the Drupal garbage collection and delete
   * the file automatically.
   *
   * @param string $log_uri
   *   The permanent log URI.
   *
   * @return \Drupal\file\FileInterface
   *   The file entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createManagedFile(string $log_uri): FileInterface {
    /** @var \Drupal\file\FileInterface $file */
    $file = $this->entityTypeManager
      ->getStorage(static::FILE_ENTITY_TYPE)
      ->create([
        'uri' => $log_uri,
        'uid' => $this->currentUser->id(),
      ]);
    $file->save();
    return $file;
  }

  /**
   * Adds log link to the import result.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity to add.
   * @param \Drupal\data_transfer\Import\ImportResultInterface $result
   *   The result to add to.
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  protected function addLogLinkToResult(
    FileInterface $file,
    ImportResultInterface $result
  ): void {
    $url = file_create_url($file->getFileUri());
    $title = $this->getSetting(static::LOG_LINK_TEXT_SETTING);
    $result->addLink($title, $url);
  }

  /**
   * A destructor.
   */
  public function __destruct() {
    // Just to make sure we're not loosing any errors.
    $this->writeErrorBuffersToTempFile();
  }

}
