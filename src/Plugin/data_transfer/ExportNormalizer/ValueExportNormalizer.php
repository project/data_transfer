<?php

namespace Drupal\data_transfer\Plugin\data_transfer\ExportNormalizer;

use Drupal\data_transfer\Export\Normalizer\ExportNormalizerBase;

/**
 * Plain value normalizer.
 *
 * It takes value from the source record and puts it to the result record.
 * Available configuration options:
 * - from: the property path in the source record, properties are separated with
 *   a dot.
 * - to: the property path in the result record, properties are separated with
 *   a dot.
 *
 * @Plugin(
 *   id = "value",
 * )
 */
class ValueExportNormalizer extends ExportNormalizerBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\data_transfer\Exception\PluginConfigurationException
   */
  public function normalize(array $record, array &$result): void {
    $value = $this->getSourceValue($record);
    $this->setTargetValue($value, $result);
  }

}
