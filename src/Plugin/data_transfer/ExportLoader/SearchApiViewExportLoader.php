<?php

namespace Drupal\data_transfer\Plugin\data_transfer\ExportLoader;

use Drupal\Core\Entity\EntityInterface;
use Drupal\search_api\Plugin\views\query\SearchApiQuery;
use Drupal\data_transfer\Export\Loader\EntityWrapperTrait;
use Drupal\data_transfer\Export\Loader\ViewsExportLoaderBase;

/**
 * Loader that retrieves source data from a Search API view.
 *
 * It requires the view to use special display and style plugins.
 *
 * The list of options:
 * - view: (required) The view name to use as data source,
 * - display: (required) The view display ID to use.
 *
 * The loader reads the following keys in the input data:
 * - filters: exposed input to pass to the view.
 *
 * @Plugin(
 *   id = "search_api_view_source",
 * )
 */
class SearchApiViewExportLoader extends ViewsExportLoaderBase {

  use EntityWrapperTrait;

  /**
   * The associative array key to use for the entity in every record.
   */
  public const ENTITY_KEY = 'entity';

  /**
   * {@inheritdoc}
   */
  public function loadData(array $ids): array {
    $view = $this->getViewExecutable();
    $query = $view->getQuery();
    if (!$query instanceof SearchApiQuery) {
      throw new \LogicException(sprintf(
        'The %s view is not a Search API one.',
        $view->id()
      ));
    }

    $index = $query->getIndex();
    $items = $index->loadItemsMultiple($ids);

    $entities = [];
    foreach ($items as $item) {
      $entity = $item->getValue();
      if (!$entity instanceof EntityInterface) {
        // @todo Warning?
        continue;
      }

      $entities[] = $entity;
    }

    return $this->wrapEntities($entities);
  }

}
