<?php

namespace Drupal\data_transfer\Plugin\data_transfer\ImportProcessor;

use Drupal\data_transfer\Import\Processor\ImportProcessorBase;
use Drupal\data_transfer\Import\Record\ImportRecordInterface;

/**
 * Plain value processor.
 *
 * It takes value from the source record and puts it to the result record.
 * Available configuration options:
 * - from: the property path in the source record, properties are separated
 *   with a dot.
 * - to: the property path in the result record, properties are separated
 *   with a dot.
 *
 * @Plugin(
 *   id = "value",
 * )
 */
class ValueImportProcessor extends ImportProcessorBase {

  /**
   * {@inheritdoc}
   */
  protected function processRecord(ImportRecordInterface $record): void {
    $value = $this->getSourceValue($record);
    $this->setTargetValue($record, $value);
  }

}
