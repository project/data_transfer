<?php

namespace Drupal\data_transfer\Plugin\views\display;

use Drupal\data_transfer\Plugin\views\style\SearchApiExportSourceStyle;
use Drupal\views\Plugin\views\display\DisplayPluginBase;

/**
 * The export source view display plugin.
 *
 * Provides data to the exporter.
 *
 * @ViewsDisplay(
 *   id = \Drupal\data_transfer\Plugin\views\display\ExportSourceDisplay::PLUGIN_ID,
 *   title = @Translation("Export source"),
 *   admin = @Translation("Export source"),
 *   help = @Translation("Provides entity IDs for any kind of export."),
 * )
 *
 * @see \Drupal\data_transfer\Export\Loader\ViewsExportLoaderBase
 */
class ExportSourceDisplay extends DisplayPluginBase {

  /**
   * The plugin ID.
   */
  public const PLUGIN_ID = 'data_transfer_export_source';

  /**
   * The display type.
   */
  public const DISPLAY_TYPE = 'data_transfer_export_source';

  /**
   * Name of the export options passed from the export loader.
   */
  public const EXPORT_OPTIONS_KEY = 'export_options';

  /**
   * The limit key in the export options.
   */
  public const LIMIT_OPTION_KEY = 'limit';

  /**
   * The offset key in the export options.
   */
  public const OFFSET_OPTION_KEY = 'offset';

  /**
   * The key in the export options that enables the total counting.
   */
  public const GET_TOTAL_OPTION_KEY = 'get_total';

  /**
   * The map of the pagers supported by this display.
   */
  public const SUPPORTED_PAGERS = [
    'some' => TRUE,
  ];

  /**
   * The views style plugin ID to use by default on this display.
   */
  protected const DEFAULT_STYLE_PLUGIN = SearchApiExportSourceStyle::PLUGIN_ID;

  /**
   * {@inheritdoc}
   */
  protected $usesAJAX = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesPager = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesAttachments = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesAreas = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesMore = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    // Force the style plugin to a custom one.
    $options['style']['contains']['type'] = [
      'default' => static::DEFAULT_STYLE_PLUGIN,
    ];
    $options['defaults']['default']['style'] = FALSE;

    // Set the display title to an empty string (not used in this display type).
    $options['title']['default'] = '';
    $options['defaults']['default']['title'] = FALSE;

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options) {
    parent::optionsSummary($categories, $options);

    // Disable 'title' so it won't be changed from the default set in
    // ::defineOptions().
    unset($options['title']);
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return static::DISPLAY_TYPE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    return $this->view->render($this->display['id']);
  }

  /**
   * Builds the view result as a renderable array.
   *
   * @return array
   *   Renderable array or empty array.
   */
  public function render() {
    if (!empty($this->view->result) || $this->view->style_plugin->evenEmpty()) {
      return $this->view->style_plugin->render($this->view->result);
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function usesExposed() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if (!empty($this->view->live_preview)) {
      $this->view->setOffset(0);
      $this->view->setItemsPerPage(10);
      return;
    }

    $options = $this->getOption(static::EXPORT_OPTIONS_KEY);
    $get_total = $options[static::GET_TOTAL_OPTION_KEY] ?? FALSE;
    $offset = $options[static::OFFSET_OPTION_KEY] ?? NULL;
    $limit = $options[static::LIMIT_OPTION_KEY] ?? NULL;

    if (!empty($get_total)) {
      $this->view->get_total_rows = TRUE;
    }
    if ($offset !== NULL) {
      $this->view->setOffset($offset);
    }
    if ($limit !== NULL) {
      $this->view->setItemsPerPage($limit);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $errors = parent::validate();

    $pager = $this->getPlugin('pager');
    if (!$pager) {
      $errors[] = $this->t(
        'The display requires a pager in order to limit the results number.'
      );
    }
    else {
      $pager_id = $pager->getPluginId();
      if (!isset(static::SUPPORTED_PAGERS[$pager_id])) {
        $errors[] = $this->t('The pager type is not supported by the display.');
      }
    }

    return $errors;
  }

}
