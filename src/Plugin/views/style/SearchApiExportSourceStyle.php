<?php

namespace Drupal\data_transfer\Plugin\views\style;

use Drupal\search_api\Item\ItemInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * The Search API view style plugin that provides data to the export loader.
 *
 * @ViewsStyle(
 *   id = \Drupal\data_transfer\Plugin\views\style\SearchApiExportSourceStyle::PLUGIN_ID,
 *   title = @Translation("Search API export source IDs"),
 *   help = @Translation("Returns an array of IDs for export."),
 *   display_types = {
 *     "data_transfer_export_source",
 *   },
 * )
 */
class SearchApiExportSourceStyle extends StylePluginBase {

  /**
   * The plugin ID.
   */
  public const PLUGIN_ID = 'data_transfer_search_api_export_source';

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesFields = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = FALSE;

  /**
   * {@inheritdoc}
   */
  public function render() {
    if (!empty($this->view->live_preview)) {
      return parent::render();
    }

    // Group the rows according to the grouping field, if specified.
    $sets = $this->renderGrouping($this->view->result, $this->options['grouping']);

    $item_ids = [];
    foreach ($sets as $records) {
      foreach ($records as $values) {
        $item = $values->_item;
        if (!$item instanceof ItemInterface) {
          continue;
        }

        $item_ids[] = $item->getId();
      }
    }

    // The method returns render array, so we pass data in properties to avoid
    // any issues.
    return [
      '#total' => $this->view->total_rows,
      '#ids' => $item_ids,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function evenEmpty() {
    return TRUE;
  }

}
