<?php

namespace Drupal\data_transfer\Plugin;

/**
 * Represents plugin specification.
 *
 * @see \Drupal\data_transfer\Plugin\PluginSpecificationInterface
 */
class PluginSpecification implements PluginSpecificationInterface {

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * The plugin configuration.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * A constructor.
   *
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $configuration
   *   The plugin configuration.
   */
  public function __construct(string $plugin_id, array $configuration = []) {
    $this->pluginId = $plugin_id;
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId(): string {
    return $this->pluginId;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

}
