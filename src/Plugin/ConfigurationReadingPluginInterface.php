<?php

namespace Drupal\data_transfer\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Represents a plugin that needs to parse its configuration on creation.
 */
interface ConfigurationReadingPluginInterface extends PluginInspectionInterface {

  /**
   * Reads the plugin configuration into its properties for later use.
   *
   * It gets called by the plugin manager right after creating an instance.
   *
   * @see \Drupal\data_transfer\Plugin\PluginManagerBase::createFromSpecification()
   */
  public function readConfiguration(): void;

}
