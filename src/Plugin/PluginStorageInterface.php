<?php

namespace Drupal\data_transfer\Plugin;

/**
 * The persistent plugin storage.
 *
 * The storage should be used by plugins that require to persist some data even
 * between batch requests on long running exports.
 *
 * @see \Drupal\data_transfer\Plugin\PluginWithStorageInterface
 */
interface PluginStorageInterface {

  /**
   * Reads value from the storage.
   *
   * @param string|array $key
   *   The key to read. Could be an array to access nested value.
   *
   * @return mixed
   *   The value or NULL if not found.
   */
  public function get($key);

  /**
   * Sets the value.
   *
   * @param string|array $key
   *   The key to write to. Could be an array to access nested value.
   * @param mixed $value
   *   The value to write.
   *
   * @return static
   *   Self.
   */
  public function set($key, $value): PluginStorageInterface;

  /**
   * Un-sets value at the specified key.
   *
   * @param string|array $key
   *   The key to un-set. Could be an array to access nested value.
   *
   * @return static
   *   Self.
   */
  public function unset($key): PluginStorageInterface;

  /**
   * Checks if the specified key exists in the storage.
   *
   * @param string|array $key
   *   The key to check. Could be an array to check nested value.
   *
   * @return bool
   *   TRUE if the key exists in the storage.
   */
  public function isset($key): bool;

  /**
   * Checks if the storage is empty.
   *
   * @return bool
   *   TRUE if the storage is empty and shouldn't be actually saved.
   */
  public function isEmpty(): bool;

  /**
   * Returns array representation of the storage.
   *
   * Result of this method could be passed to the class constructor in order to
   * re-create exactly the same storage. It should be used to persist the
   * storage between requests.
   *
   * @return array
   *   The array representation of the storage.
   */
  public function toArray(): array;

}
