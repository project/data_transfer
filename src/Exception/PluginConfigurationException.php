<?php

namespace Drupal\data_transfer\Exception;

/**
 * Thrown in case the plugin configuration is broken.
 */
class PluginConfigurationException extends ExchangeException {

}
