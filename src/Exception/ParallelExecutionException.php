<?php

namespace Drupal\data_transfer\Exception;

/**
 * Thrown in case multiple executions of the same export are detected.
 */
class ParallelExecutionException extends ExchangeException {

}
