<?php

namespace Drupal\data_transfer\Exception;

/**
 * Base class and generic exception for the import/export failures.
 */
class ExchangeException extends \Exception {

}
