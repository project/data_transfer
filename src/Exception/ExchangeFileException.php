<?php

namespace Drupal\data_transfer\Exception;

/**
 * Thrown in case the file opening/reading/writing/closing fails.
 */
class ExchangeFileException extends ExchangeException {

}
