<?php

namespace Drupal\data_transfer\Exception;

/**
 * Exception triggered to skip row processing.
 *
 * The exception message is visible to the user, so be careful with sensitive
 * data.
 */
class ExchangeSkipRowException extends ExchangeException {

}
