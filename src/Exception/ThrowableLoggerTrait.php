<?php

namespace Drupal\data_transfer\Exception;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Utility\Error;
use Psr\Log\LoggerInterface;

/**
 * Provides trait for classes that catch and log errors/exceptions.
 */
trait ThrowableLoggerTrait {

  /**
   * Returns the throwable logger.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger.
   */
  abstract protected function getThrowableLogger(): LoggerInterface;

  /**
   * Logs the throwable.
   *
   * @param \Throwable $e
   *   The throwable to log.
   *
   * @see watchdog_exception()
   *
   * @todo Use watchdog_exception() or its replacement when it supports any
   *   throwable and not just exceptions.
   */
  protected function logThrowable(\Throwable $e): void {
    $message = '%type: @message in %function (line %line of %file). Backtrace: @backtrace_string';
    $variables = Error::decodeException($e);

    $this->getThrowableLogger()
      ->log(RfcLogLevel::ERROR, $message, $variables);
  }

}
